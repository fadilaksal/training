###################
Polres Apps Backend - (Beta)
###################

![Dashboard](https://s10.postimg.org/ltvfki5u1/Screen_Shot_2016_08_13_at_12_05_32_PM.png)

*******************
List of Used Plugins
*******************
* Grocery CRUD (http://www.grocerycrud.com)
* Admin LTE 
* Smarty (http://www.smarty.net)
* Custom Model (JamieRumbelow) (https://github.com/jamierumbelow/codeigniter-base-model)
* REST Api Controller (https://github.com/chriskacerguis/codeigniter-restserver)
* Grocery CRUD Bootstrap Themes
* ION Auth (http://benedmunds.com/ion_auth/)