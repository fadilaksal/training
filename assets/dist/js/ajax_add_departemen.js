    $("#formInput").submit(function(e) {

    var url = "http://localhost/training/departemen/proses_add"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#formInput").serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Berhasil menambahkan Departemen', 'success'); 
              }else{
                swal('Gagal', 'Gagal menambahkan Departemen', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal menambahkan Departemen', 'error');
          }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});