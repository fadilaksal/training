$("#cari").click(function(e) {
  $('#tableTraining').show();
  $('#load').html('<div class="loader"></div>');
  var url = "http://localhost/training/training/getTrainingForAdd"; // url nya di controller training dengan nama fungsi getTrainingForAdd

  $.ajax({
         type: "POST",
         url: url,
         data: {
            'nama': $("#InputName").val()
         }, // serializes the form's elements.
         success: function(data)
         {
            $('#dataTraining').html(data); // id dataTraining diisi dengan hasil dari fungsi getTrainingForAdd yang ada di controller Training
         },
         error: function(xhr, textStatus, errorThrown){
            swal('Gagal', 'Data tidak ditemukan', 'error');
        }
       });

  e.preventDefault(); // avoid to execute the actual submit of the form.
});

$("#formInput").submit(function(e) {
var url = "http://localhost/training/training/proses_add_karyawan"; // the script where you handle the form input.
$('#load2').html('<div style="float:left; margin-left: 40px; padding:10px 0;">Proses input...</div><div class="loader"></div>');
$('#btnTambah').prop('disabled', true);
$.ajax({
       type: "POST",
       url: url,
       data: $("#formInput").serialize(), // serializes the form's elements.
       success: function(data)
       {
          if(data == 'true'){
           swal('Sukses', 'Berhasil menambahkan Karyawan', 'success'); 
           clear();
          }else{
            swal('Gagal', 'Gagal menambahkan Karyawan', 'error');
            clear();
          }
       },
       error: function(xhr, textStatus, errorThrown){
          swal('Gagal', 'Gagal menambahkan Karyawan', 'error');
          clear();
      }
     });

e.preventDefault(); // avoid to execute the actual submit of the form.
});

function clear() {
  $('#load2').html('');
  $('#btnTambah').prop('disabled', false);
}