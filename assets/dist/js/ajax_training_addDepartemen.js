$("#cari").click(function(e) {
  $('#tableTraining').show();
  $('#load').html('<div class="loader"></div>');
  var url = "http://localhost/training/training/getTrainingForAdd"; // url nya di controller training dengan nama fungsi getTrainingForAdd

  $.ajax({
         type: "POST",
         url: url,
         data: {
            'nama': $("#InputName").val()
         }, // serializes the form's elements.
         success: function(data)
         {
            $('#dataTraining').html(data); // id dataTraining diisi dengan hasil dari fungsi getTrainingForAdd yang ada di controller Training
         },
         error: function(xhr, textStatus, errorThrown){
            swal('Gagal', 'Data tidak ditemukan', 'error');
        }
       });

  e.preventDefault(); // avoid to execute the actual submit of the form.
});


$("#formInput").submit(function(e) {
var url = "http://localhost/training/training/proses_add_departemen"; // the script where you handle the form input.
$.ajax({
       type: "POST",
       url: url,
       data: $("#formInput").serialize(), // serializes the form's elements.
       success: function(data)
       {
          if(data == 'true'){
            $("#btnLanjut").css('visibility', 'visible');
           swal('Sukses', 'Berhasil menambahkan Departemen', 'success'); 
          }else{
            swal('Gagal', 'Gagal menambahkan Departemen', 'error');
          }
       },
       error: function(xhr, textStatus, errorThrown){
          swal('Gagal', 'Gagal menambahkan Departemen', 'error');
      }
     });

e.preventDefault(); // avoid to execute the actual submit of the form.
});