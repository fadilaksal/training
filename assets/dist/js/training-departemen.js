//console.log('test');
// var ctx = document.getElementById('barChart').getContext('2d');
var ctx2 = document.getElementById('barChart2').getContext('2d');
var dataJS = JSON.parse(document.currentScript.dataset.dataJS);
var statistikHadir = dataJS.statistikHadir;
var statistikKelulusan = dataJS.statistikKelulusan;
//console.log(statistikKelulusan);
var tidakHadir = (statistikHadir.tidakHadir / statistikHadir.jumlah * 100).toFixed(2);
var hadir = (statistikHadir.hadir / statistikHadir.jumlah * 100).toFixed(2);
var dataHadir = [hadir, tidakHadir];
var labelHadir = ['Hadir', 'Tidak Hadir'];

var lulus = (statistikKelulusan.lulus / statistikKelulusan.jumlah * 100).toFixed(2);
var tidak = (statistikKelulusan.tidak / statistikKelulusan.jumlah * 100).toFixed(2);

var dataLulus = [lulus, tidak];
var labelLulus = ['Lulus', 'Tidak Lulus'];

var options = {
      hover: {
          animationDuration: 0
      }, 
      scales: {
        xAxes:[{
            gridLines: {
              display:false
            }
        }],
        yAxes:[{
          ticks: {
              suggestedMin: 0,
              suggestedMax: 100,
              stepSize:20
          },
          stacked:true
        }]
      },
      animation: {
        duration: 1,
        onComplete: function () {
          var chartInstance = this.chart,
              ctx = chartInstance.ctx;
          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];                            
                  ctx.fillText(data + "%", bar._model.x, bar._model.y - 5);
              });
          });
      }
    },responsive:true
  };
var dataOption = {
    type: 'bar',

    data: {
         labels: labelHadir,
          datasets: [
            {
              label: "Presentasi kehadiran",
              backgroundColor: [
                'rgba(0, 180, 99, 0.6)',
                'rgba(255,88,132,0.6)'
              ],
              borderColor: [
                'rgba(0, 132, 99, 1)',
                'rgba(255,20,132,1)'],
              borderWidth: 2,
              data: dataHadir
            }
          ]
    },
    options : options
};

var dataOption2 = {
    type: 'bar',

    data: {
         labels: labelLulus,
          datasets: [
            {
              label: "Presentasi kelulusan",
              backgroundColor: [
                'rgba(0, 180, 99, 0.6)',
                'rgba(255,88,132,0.6)'
              ],
              borderColor: [
                'rgba(0, 132, 99, 1)',
                'rgba(255,20,132,1)'],
              borderWidth: 2,
              data: dataLulus
            }
          ]
    },
    options : options
};
// var chart = new Chart(ctx, dataOption);
var chart2 = new Chart(ctx2, dataOption2);

$('#barChart').click(
    function(evt){
       var activePoints = chart.getElementsAtEvent(evt);
       var firstPoint = activePoints[0];
        if (firstPoint !== undefined){
          //var jumlah = dataOption.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
          //jumlah = dataOption.data.labels[firstPoint._index];
          //console.log(jumlah);
          location.href="training-departemen.html";
        }
    }
  );

$('.openDialog').click(
  function (evt) {
    var id = $(this).attr("data-id");
    var nama = $(this).closest('tr').children('td.employee_name').text();
    var employee_ID = $(this).closest('tr').children('td.employee_ID').text();
    var kehadiran = $(this).closest('tr').children('td.kehadiran').text();
    var pretest = $(this).closest('tr').children('td.pretest').text();
    var posttest = $(this).closest('tr').children('td.posttest').text();
    var status = $(this).closest('tr').children('td.status').text();
    
    $("input#id").val( id );
    $("input#InputName").val( nama );
    $("input#InputEmployeeID").val( employee_ID );
    $("input#InputKehadiran").val( kehadiran );
    $("input#InputPreTest").val( pretest );
    $("input#InputPostTest").val( posttest );
    $("input#InputStatus").val( status );
    //console.log(id + nama);
  });

$('#formEdit').submit(function(e) {

    var url = "http://localhost/training/training/proses_edit_training_karyawan"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#formEdit").serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Berhasil menambahkan Training', 'success').then(function(){
                location.reload();
               }); 

              }else{
                swal('Gagal', 'Gagal menambahkan Training', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal menambahkan Training', 'error');
          }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});