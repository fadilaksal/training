$("#tableTraining").hide();
$("#mytable #checkall").click(function () {
    if ($("#mytable #checkall").is(':checked')) {
        $("#mytable input[type=checkbox]").each(function () {
            $(this).prop("checked", true);
        });

    } else {
        $("#mytable input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
        });
    }
});

$("[data-toggle=tooltip]").tooltip();

function setIdTraining(id, nama){
	$('#idTraining').val(''+id);
	$('#InputName').val(''+nama);
	$("#tableTraining").hide();
}
