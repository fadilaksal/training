    $("#formInput").submit(function(e) {

    var url = "http://localhost/training/departemen/proses_edit"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#formInput").serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Departemen berhasil diubah', 'success'); 
              }else{
                swal('Gagal', 'Dapartemen gagal diubah', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal menambahkan Training', 'error');
          }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});