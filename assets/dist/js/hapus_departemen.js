$('#btnHapus').click(function(){
	var id = $('#idDepartemen').val();
	console.log(id);
	swal({
	  title: 'Hapus Departemen',
	  type: 'info',
	  html:
	    'anda yakin ingin menghapus Departemen ini ?',
	  confirmButtonColor: '#d33',
	  showCloseButton: true,
	  showCancelButton: true,
	  confirmButtonText:
	    '<i class="fa fa-check"></i> Hapus!',
	  cancelButtonText:
	    'Cancel'
	}).then(function () {
	  hapusDepartemen(id);
	});
});

function hapusDepartemen(id){

    var url = "http://localhost/training/departemen/hapus"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: {'id' : id}, // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Berhasil menghapus Departemen', 'success').then(function (){
               	location.href="http://localhost/training/dashboard";
               });
              }else{
                swal('Gagal', 'Gagal menghapus Departemen', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal menghapus Departemen', 'error');
          }
         });
};