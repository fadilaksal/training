var ctx = document.getElementById('barChart').getContext('2d');
var ctx2 = document.getElementById('barChart2').getContext('2d');
// var ctx3 = document.getElementById('barChart3').getContext('2d');
var dataJS = JSON.parse(document.currentScript.dataset.dataJS);
//console.log(dataJS);
var bigFiveDepartment = dataJS.bigFiveDepartment;
var lastYearTraining = dataJS.lastYearTraining;

var opt = {
  title: {
    display: false,
    text: 'Data Training per 2017',
    position: 'bottom'
  },
  hover: {
      animationDuration: 0
  }, 
  scales: {
    xAxes:[{
        gridLines: {
          display:false
        }
    }],
    yAxes:[{
      ticks: {
          suggestedMin: 0,
          suggestedMax: 10
      },
      stacked:true
    }]
  },
  animation: {
      duration: 1,
      onComplete: function () {
          var chartInstance = this.chart,
              ctx = chartInstance.ctx;
          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];                            
                  ctx.fillText(data, bar._model.x, bar._model.y - 5);
              });
          });
      }
  }
};

var label = [];
var jumlahTraining = [];

var training = {bulan : '', jumlah:0}
var bulan = [];
for (var i = 0; i < 12; i++) {
  bulan[i] = jQuery.extend(true, {}, training);
}
bulan[0].bulan = "January";
bulan[1].bulan = "February";
bulan[2].bulan = "March";
bulan[3].bulan = "April";
bulan[4].bulan = "Mei";
bulan[5].bulan = "June";
bulan[6].bulan = "July";
bulan[7].bulan = "August";
bulan[8].bulan = "September";
bulan[9].bulan = "October";
bulan[10].bulan = "November";
bulan[11].bulan = "December";
//console.log(bulan);
for (var i = 0; i < 12; i++) {
  //console.log(bulan[i]);
  for (var key in lastYearTraining) {
    if(bulan[i].bulan == lastYearTraining[key].bulan){
      bulan[i].jumlah = lastYearTraining[key].jumlah;
    }
  }
}

for(var i = 0; i<12;i++){
  label[i] = bulan[i].bulan;
  jumlahTraining[i] = bulan[i].jumlah;
}
//console.log(bulan);
// var i = 0;
// for (var key in lastYearTraining) {
//   //console.log(dataJS[key]);
//   //console.log(lastYearTraining[key]);
//   label[i] = lastYearTraining[key].bulan;
//   jumlahTraining[i] = lastYearTraining[key].jumlah;
//   i++;
// }
//console.log(label);

var dataOption = {
    type: 'bar',
    data: {
         labels: label,
          datasets: [
            {
              label: "Jumlah Training",
              backgroundColor: "rgba(255,99,132,1)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 2,
              hoverBackgroundColor: "rgba(200,99,132,1)",
              hoverBorderColor: "rgba(255,99,132,1)",
              data: jumlahTraining
            }
          ]
    },
    options: opt
};
var chart = new Chart(ctx, dataOption);
var i = 0;
label = [];
jumlahTraining = [];
for (var key in bigFiveDepartment) {
    label[i] = bigFiveDepartment[key].departemen;
    jumlahTraining[i] = bigFiveDepartment[key].jumlahTraining;
    i++;
  }

var opt2 = {
  title: {
    display: true,
    text: 'Data Training per Departemen',
    position: 'bottom'
  },
  hover: {
      animationDuration: 0
  }, 
  scales: {
    xAxes:[{
        gridLines: {
          display:false
        },
        ticks: {
          stepSize: 1,
          min: 0,
          autoSkip: false
        }
    }],
    yAxes:[{
      ticks: {
          suggestedMin: 0,
          suggestedMax: 10
      },
      stacked:true
    }]
  },
  animation: {
      duration: 1,
      onComplete: function () {
          var chartInstance = this.chart,
              ctx = chartInstance.ctx;
          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];                            
                  ctx.fillText(data, bar._model.x, bar._model.y - 5);
              });
          });
      }
  }
};

opt.title.text = "Data Training per Departemen";
opt.title.display = true;
var dataOption2 = {
    type: 'bar',
    data: {
         labels: label,
          datasets: [
            {
              label: "Jumlah Training",
              backgroundColor: "#3c8dbc",
              borderColor: "rgba(132,80,255,1)",
              borderWidth: 2,
              hoverBackgroundColor: "rgba(132,80,255,1)",
              hoverBorderColor: "rgba(132,80,255,1)",
              data: jumlahTraining
            }
          ]
    },
    options: opt2
};
var chart = new Chart(ctx2, dataOption2);
var dataOption3 =$.extend(true, {}, dataOption2);
dataOption3.data.datasets[0].borderColor = "rgba(102,200,98,1)";
dataOption3.data.datasets[0].hoverBackgroundColor = "rgba(102,200,98,1)";
dataOption3.data.datasets[0].hoverBorderColor = "rgba(102,200,98,1)";
// var chart = new Chart(ctx3, dataOption3);

$('#barChart').click(
    function(evt){
       var activePoints = chart.getElementsAtEvent(evt);
       var firstPoint = activePoints[0];
        if (firstPoint !== undefined){
          //var jumlah = dataOption.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
          //jumlah = dataOption.data.labels[firstPoint._index];
          //console.log(jumlah);
          // location.href="training-id.html";
        }
    }
  );

var totalBiaya = formattingRupiah($('#totalBiaya').text());
$('#totalBiaya').html(totalBiaya);
function formattingRupiah(value){
  num = parseFloat(value.toString());
  var num = 'Rp. ' + num.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
  //return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
  return num;
}