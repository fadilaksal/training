    $("#formInput").submit(function(e) {

    var url = "http://localhost/training/admin/proses_edit/"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#formInput").serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Berhasil mengubah akun', 'success'); 
               $("#btnLanjut").css('visibility', 'visible');

              }else{
                swal('Gagal', 'Gagal mengubah akun', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal mengubah akun', 'error');
          }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});
