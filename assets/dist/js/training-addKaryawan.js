$("#tableTraining").hide();
$("#mytable #checkall").click(function () {
    if ($("#mytable #checkall").is(':checked')) {
        $("#mytable input[type=checkbox]").each(function () {
            $(this).prop("checked", true);
        });

    } else {
        $("#mytable input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
        });
    }
});

$("[data-toggle=tooltip]").tooltip();

$("#inputDepartemen").change(function(){
	console.log(this.value);
    getKaryawan();
});

function setIdTraining(id, nama){ //ini dipanggil di training-data.html
	$('#idTraining').val(''+id);
	$('#InputName').val(''+nama);
	$("#tableTraining").hide();
    $("#inputDepartemen").prop('disabled', false);
    getDepartemen();
}

function getDepartemen(){
    var url = "http://localhost/training/training/getDepartemenForTraining"; // url nya di controller training dengan nama fungsi getTrainingForAdd
    console.log($("#idTraining").val())
    id = $("#idTraining").val();
    $.ajax({
         type: "POST",
         url: url,
         data: {
            'id': id
         }, // serializes the form's elements.
         success: function(data)
         {
            $('#inputDepartemen').html(data); // id dataTraining diisi dengan hasil dari fungsi getTrainingForAdd yang ada di controller Training
         },
         error: function(xhr, textStatus, errorThrown){
            swal('Gagal', 'Data tidak ditemukan', 'error');
        }
    });
}

function getKaryawan(){
    $('#dataKaryawan').html('<div class="loader"></div>');
    var url = "http://localhost/training/training/getKaryawanForTraining"; // url nya di controller training dengan nama fungsi getTrainingForAdd
    //console.log($("#idTraining").val())
    departemen = $("#inputDepartemen").val();
    $.ajax({
         type: "POST",
         url: url,
         data: {
            'departemen': departemen
         }, // serializes the form's elements.
         success: function(data)
         {
            $('#dataKaryawan').html(data); // id dataTraining diisi dengan hasil dari fungsi getTrainingForAdd yang ada di controller Training
         },
         error: function(xhr, textStatus, errorThrown){
            swal('Gagal', 'Data tidak ditemukan', 'error');
        }
    });
}
$(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).removeClass("btn-default").addClass("btn-primary");   
});