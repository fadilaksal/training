$("#formInput").validate({
    ignore: ":hidden",
    rules: {
      email: {required:true, minlength:5,email:true},
      username: {required:true, minlength:5},
      firstname: {required:true, minlength:3},
      lastname: {required:true, minlength:3},
      password: {required:true, minlength:8},
      confirmation: {required:true, minlength:8, equalTo: "#inputPassword"}
    },submitHandler: function(form){
        var url = "http://localhost/training/admin/proses_add/"; 

        $.ajax({
           type: "POST",
           url: url,
           data: $("#formInput").serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data == 'true'){
               swal('Sukses', 'Berhasil menambahkan akun', 'success'); 
               $("#btnLanjut").css('visibility', 'visible');

              }else{
                swal('Gagal', 'Gagal menambahkan akun', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal menambahkan akun', 'error');
          } 
         });
         return false;
         e.preventDefault(); 
     }
});

jQuery.extend(jQuery.validator.messages, {
  email: "Masukkan email dengan benar",
  equalTo: "Password harus sama",
  minlength: "Masukkan minimal {0} karakter"
});