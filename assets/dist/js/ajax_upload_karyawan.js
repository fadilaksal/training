$("#formInput").submit(function(e) {
    $('#loader').html('<div class="loader2"></div>');
    $("#buttonSubmit").prop('disabled', true);
    if( document.getElementById("file-7").files.length == 0 ){
      clear('<strong> * Pilih file terlebih dahulu</strong>');
      return false;
    }
    
    var filename = document.getElementById("file-7").files[0].name;
    //console.log();
    if(filename.split('.').pop() != 'csv'){
      clear('<strong> * File harus bertipe CSV </strong>');
      return false;
    }

    //console.log('test');
    var url = "http://localhost/training/karyawan/proses_upload"; // the script where you handle the form input.
    
    $.ajaxFileUpload({
           type: "POST",
           url: url,
           data: $("#formInput").serialize(), // serializes the form's elements.
           secureuri    :false,
           fileElementId :'dataKaryawan',
           dataType    : 'json',
           success: function(data, status)
           {
              if(data.status != 'error'){
                if(data == 'true'){
                 swal('Sukses', 'Berhasil mengupload data karyawan', 'success'); 
                }else{
                  swal('Gagal', 'Gagal mengupload data karyawan', 'error');
                }
              }else{
                swal('Gagal', 'Gagal mengupload data karyawan', 'error');
              }
           },
           error: function(xhr, textStatus, errorThrown){
              swal('Gagal', 'Gagal mengupload data karyawan', 'error');
          }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

function clear(errorText) {
  $('#loader').html('');
  $("#buttonSubmit").prop('disabled', false);
  $('#errorText').html('' + errorText);
}