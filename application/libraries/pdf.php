<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require('fpdf.php');

class Pdf extends FPDF {

    // Extend FPDF using this class
    // More at fpdf.org -> Tutorials
    function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
        // Call parent constructor
        parent::__construct($orientation, $unit, $size);
        $this->fontpath = "font/";
    }

    function header_page() {
        //for Header
        $this->SetFont('Arial', '', 11);
        $this->Cell(100, 5, 'KEPOLISIAN NEGARA REPUBLIK INDONESIA', 0, 2, 'C');
        $this->Cell(100, 5, 'DAERAH JAWA TIMUR', 0, 2, 'C');
        $this->Cell(100, 5, 'RESORT NGANJUK KOTA', 0, 2, 'C');

        $this->SetLineWidth(0.25);
        $this->SetDrawColor(0, 0, 0);
        $this->line(10, 25, 110, 25);
    }
    
    function skck_profil($judul, $en_judul, $isi = ''){
        $this->SetFont('Arial', 'BU', 9);
        $this->Cell(65, 4, $judul, 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(5, 4, " : ", 0, 0, 'L');
        $this->MultiCell(110, 4, $isi, 0, 'L');
        $this->SetFont('Arial', 'I', 9);
        $this->Cell(65, 4, $en_judul, 0, 2, 'L');
    }
    
    function footer($isi = ''){
        $this->SetX(15);
        $this->Write(5, $isi);
    }
}

?>