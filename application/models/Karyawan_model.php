<?php

class Karyawan_model extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'karyawan';
		$this->primary_key = 'employee_ID';
	}
		
	public function _truncate(){
		$this->db->truncate('karyawan');
	}
}