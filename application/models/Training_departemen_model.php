<?php

class Training_Departemen_model extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'training_departemen';
	}


  public function _select($param1 = '*')
  { 
    $this->db->select($param1); 
    return $this; 
  } 
 
  public function _order_by($param1 = '', $param2 = '')
  { 
    $this->db->order_by("$param1 $param2"); 
    return $this; 
  } 

  public function _joinDepartemen()
  {
    $this->db->select('training_departemen.*, nama');
    $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
    return $this;
  }

  public function _joinDepartemenWithCount2()
  {
    $this->db->select("training_departemen.*, nama, (select count(*) from training_departemen where departemen_id = departemen.id) as 'jumlahTraining', (select count(*) from karyawan where department = nama) as 'jumlahKaryawan'");
    $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
    return $this;
  }

  public function __joinDepartemenWithCountForExportId()
  {
    $this->db->select("training.nama as 'nama training', departemen.nama as namaDepartemen, (select count(*) from training_departemen where departemen_id = departemen.id) as 'jumlahTraining', 
      (select count(*) from karyawan where department = departemen.nama) as 'jumlahKaryawan', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = departemen.nama) as 'jumlahPeserta', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = departemen.nama and a.status = 'lulus') as 'jumlahLulus'");
    $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
    $this->db->join('training', 'training_departemen.training_id = training.id', 'left');
    return $this;
  }

  public function _joinDepartemenWithCount()
  {
    $this->db->select("training_departemen.*, nama as namaDepartemen, (select count(*) from training_departemen where departemen_id = departemen.id) as 'jumlahTraining', 
      (select count(*) from karyawan where department = nama) as 'jumlahKaryawan', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama) as 'jumlahPeserta', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama and a.status = 'lulus') as 'jumlahLulus',
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama and a.kehadiran = 'hadir') as 'jumlahHadir'");
    $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
    return $this;
  }

  public function _joinDepartemenWithCountField($trainingId)
  {
    $this->db->select("training_departemen.*, nama as namaDepartemen, (select count(*) from training_departemen where departemen_id = departemen.id) as 'Jumlah Training', 
      (select count(*) from karyawan where department = nama) as 'Jumlah Karyawan', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama) as 'Jumlah Peserta', 
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama and a.status = 'lulus') as 'Jumlah Lulus',
      (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = nama and a.kehadiran = 'hadir') as 'Jumlah Hadir'");
    $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
    $this->db->where("training_departemen.training_id = $trainingId");
    return $this->db->get('training_departemen');
  }

   public function _joinDepartemenWithCountFieldForExportId($trainingId)
   {
      $this->db->select("training.nama as 'Nama Training', departemen.nama as 'Nama Departemen', (select count(*) from training_departemen where departemen_id = departemen.id) as 'Jumlah Training', 
        (select count(*) from karyawan where department = departemen.nama) as 'Jumlah Karyawan', 
        (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = departemen.nama) as 'Jumlah Peserta', 
        (select count(*) from training_karyawan a, karyawan b where a.employee_id = b.employee_ID and a.training_id = training_departemen.training_id and b.department = departemen.nama and a.status = 'lulus') as 'Jumlah Lulus'");
      $this->db->join('departemen', 'training_departemen.departemen_id = departemen.id', 'left');
      $this->db->join('training', 'training_departemen.training_id = training.id', 'left');
      $this->db->where("training_departemen.training_id = $trainingId");
      return $this->db->get('training_departemen');
  }

  public function _joinTraining()
  {
    $this->db->select("*, DATE_FORMAT(tanggal,'%d-%m-%Y') as tanggalTraining");
    $this->db->join('training', 'training.id = training_departemen.training_id', 'left');
    return $this;
  }

  // public function joinKaryawan($param1 = '*'){
  // 	$this->db->select("departemen.*, count(Employee_name) as 'jumlahKaryawan'"); 
  // 	$this->db->join('karyawan', 'departemen.nama = karyawan.Department', 'left')
  // 				->group_by('departemen.nama');

  //   return $this; 
  // }
}