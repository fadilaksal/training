<?php

class Trainer_model extends MY_Model {
	public function __construct()
		{
			parent::__construct();
			$this->_table = 'trainer';
		}
	public function getTrainingByIdKaryawan($idTrainer = 0){
		$this->db->select('trainer.*, training.*');
		$this->db->from('trainer');
		$this->db->join('training', 'training.id = trainer.training_id', 'left');
		$this->db->where("trainer.employee_id = $idTrainer");
		return $this->db->get();
	}

	public function getTrainerByIdTraining($idTraining = 0){
		$this->db->select('karyawan.employee_name');
		$this->db->from('trainer');
		$this->db->join('karyawan', 'karyawan.employee_ID = trainer.employee_id', 'left');
		$this->db->where("trainer.training_id = $idTraining");
		return $this->db->get();
	}

	public function getTotalDurasi($idTrainer){
		$this->db->select('sum(training.durasi) total');
		$this->db->from('trainer');
		$this->db->join('training', 'training.id = trainer.training_id', 'left');
		$this->db->where("trainer.employee_id = $idTrainer");
		return $this->db->get();
	}
}