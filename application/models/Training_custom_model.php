<?php

class Training_custom_model extends MY_Model {
	public function __construct()
		{
			parent::__construct();
			$this->_table = 'training_custom';
		}

	public function _select($param1 = '*'){
		$this->db->select($param1);
		return $this;
	}

	//$param1 = kolom, $param2 = asc/desc
	public function _order_by($param1 = '', $param2 = ''){
		$this->db->order_by("$param1 $param2");
		return $this;
	}

	public function _limit($limit, $offset){
		$this->db->limit($limit, $offset);
		return $this;
	}

	public function group_by($param1){
		$this->db->group_by($param1);
		return $this;
	}

	//$param1 = nama field, $param2 = yang dicari
	public function _like($param1 = '', $param2 = ''){
		$this->db->like($param1, $param2);
		return $this;
	}
}