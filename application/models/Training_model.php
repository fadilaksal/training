<?php

class Training_model extends MY_Model {
	public function __construct()
		{
			parent::__construct();
			$this->_table = 'training';
		}

	public function _select($param1 = '*'){
		$this->db->select($param1);
		return $this;
	}

	//$param1 = kolom, $param2 = asc/desc
	public function _order_by($param1 = '', $param2 = ''){
		$this->db->order_by("$param1 $param2");
		return $this;
	}

	public function _limit($limit, $offset){
		$this->db->limit($limit, $offset);
		return $this;
	}

	public function group_by($param1){
		$this->db->group_by($param1);
		return $this;
	}

	//$param1 = nama field, $param2 = yang dicari
	public function _like($param1 = '', $param2 = ''){
		$this->db->like($param1, $param2);
		return $this;
	}

	public function getTraining(){
		$this->db->select('*');
		return $this->db->get('training');
	}


	public function selectBiayaPerTahun($tahun = 0){
		$this->db->select('sum(biaya_riil) as total');
		$this->db->where("year(tanggal) = $tahun");
		$this->db->from('training');
		$this->db->group_by('year(tanggal)');
		return $this->db->get();
	}
}