<?php

class Training_karyawan_model extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'training_karyawan';
	}


  public function _select($param1 = '*'){ 
    $this->db->select($param1); 
    return $this; 
  } 
 
  public function _order_by($param1 = '', $param2 = ''){ 
    $this->db->order_by("$param1 $param2"); 
    return $this; 
  } 

  
  public function update_multiple($data){
    $sql = '';
    foreach ($data as $key => $value) {
      $this->db->set('pre_test',$value['pre_test']);
      $this->db->set('post_test',$value['post_test']);
      $this->db->set('status',$value['status']);
      $this->db->where('training_id',(int)$value['training_id']);
      $this->db->where('employee_id',(int)$value['employee_ID']);
      $this->db->update('training_karyawan');
      // $sql .= "update training_karyawan set pre_test = '".$value['pre_test']."', post_test = '".$value['post_test']."', status = '".$value['status']."' where training_id = ".$value['training_id']." and employee_id = '".$value['employee_ID']."';";
    }
    return true;
  }

  public function _getJoinTrainingAndKaryawan($idTraining){
    $this->db->select("b.employee_ID, b.employee_name, b.department, b.line, training_karyawan.*");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    $this->db->where("c.id = $idTraining");
    return $this;
  }

  public function _getByDepartemen($param1 = 0, $param2 = ''){
    $this->db->select("b.employee_ID, b.employee_name, b.department, training_karyawan.kehadiran, training_karyawan.pre_test, training_karyawan.post_test, training_karyawan.status");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    $this->db->where("b.department = '$param2' and c.id = $param1");
    return $this;
  }

  public function _getByDepartemenWithTraining($param1 = 0, $param2 = ''){
    $this->db->select("c.id training_id, b.employee_ID, b.employee_name, b.department, training_karyawan.kehadiran, training_karyawan.pre_test, training_karyawan.post_test, training_karyawan.status, c.nama nama_training, c.tanggal, c.jenis, c.jenis_trainer, c.trainer, c.durasi");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    $this->db->where("b.department = '$param2' and c.id = $param1");
    return $this;
  }

  public function _getByDepartemenField($idTraining, $departemen = ''){
    $this->db->select("b.employee_ID as 'Employee ID', b.employee_name as 'Employee Name', b.department as 'Department', training_karyawan.kehadiran as 'Kehadiran', training_karyawan.pre_test as 'Pre Test', training_karyawan.post_test as 'Post Test', training_karyawan.status as 'Status'");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    $this->db->where("c.id = $idTraining");
    $this->db->limit(1);
    return $this->db->get('training_karyawan');
  }

  public function _getByDepartemenAllField($idTraining, $departemen = ''){
    $this->db->select("b.employee_ID, b.employee_name, b.department, b.line, training_karyawan.*");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    if($departemen != ''){
      $this->db->where("b.department = '$departemen' and c.id = $idTraining");
    }else{
      $this->db->where("c.id = $idTraining");
    }
    return $this;
  }
  public function _getByDepartemenAllField2($idTraining, $departemen = ''){
    $this->db->select("b.employee_ID, b.employee_name, b.department, training_karyawan.pre_test,training_karyawan.post_test, status");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    if($departemen == ''){
      $this->db->where("c.id = $idTraining");
    }else{
      $this->db->where("b.department = '$departemen' and c.id = $idTraining");
    }
    return $this;
  }

  public function _getByDepartemenAllJustField($idTraining, $departemen = ''){
    $this->db->select("b.employee_ID, b.employee_name, b.department, training_karyawan.pre_test,training_karyawan.post_test, status");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    if($departemen == ''){
      $this->db->where("c.id = $idTraining");
    }else{
      $this->db->where("b.department = '$departemen' and c.id = $idTraining");
    }
    return $this->db->get('training_karyawan');
  }

  public function _getByDepartemenAll($param1 = 0, $param2 = ''){
    $this->db->select("training_karyawan.*, b.employee_ID, b.employee_name,
      (select count(*) from training_karyawan JOIN karyawan on (training_karyawan.employee_id = karyawan.employee_ID) where training_karyawan.training_id = $param1 and karyawan.department = '$param2' and  training_karyawan.kehadiran = 'tidak' GROUP BY karyawan.department)  as 'Jumlah Tidak Hadir',
      (select count(*) from training_karyawan JOIN karyawan on (training_karyawan.employee_id = karyawan.employee_ID) where training_karyawan.training_id = $param1 and karyawan.department = '$param2' and training_karyawan.status = 'lulus' GROUP BY karyawan.department) as 'Jumlah Lulus',
      (select count(*) from training_karyawan JOIN karyawan on (training_karyawan.employee_id = karyawan.employee_ID) where training_karyawan.training_id = $param1 and karyawan.department = '$param2' GROUP BY karyawan.department) as 'Jumlah Peserta' ");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left')
          ->join('training c', 'training_karyawan.training_id = c.id', 'left');
    //$this->db->group_by('b.department');
    $this->db->where("b.department = '$param2' and c.id = $param1");
    return $this;
  }

  public function _getJustDepartemen(){
    $this->db->select("c.nama, COUNT(*) as 'jumlahTraining', b.department");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left');
    $this->db->join('training c', 'training_karyawan.id = c.id', 'left');
    $this->db->group_by('b.department');
    return $this;
  }

  public function _getDepartemenRightJoin($trainingId = 0){
    $this->db->select("b.department, d.id as id_departemen");
    $this->db->join('karyawan b', 'training_karyawan.employee_id = b.employee_ID', 'left');
    $this->db->join('departemen d', 'd.nama = b.department', 'right');
    if($trainingId != 0){
      $this->db->where("training_karyawan.training_id = $trainingId");
    }
    $this->db->group_by('b.department');
    return $this;
  }

  public function _limit($limit, $offset){
    $this->db->limit($limit, $offset);
    return $this;
  }

  public function _joinTraining(){
    $this->db->select("training_karyawan.*, training.nama as training_name, DATE_FORMAT(tanggal,'%d-%m-%Y') as tanggal, training.kategori, training.jenis");
    $this->db->join('training','training_karyawan.training_id = training.id', 'left');
    return $this;
  }

  public function _countKehadiran($training_id){
    $this->db->select("(select count(id) from training_karyawan where training_id = $training_id and kehadiran = 'tidak') as tidakHadir, (select count(id) from training_karyawan where training_id = '$training_id' and kehadiran = 'hadir') as hadir, (select count(id) from training_karyawan where training_id = $training_id) as jumlah");
    return $this->db->get()->result();
  }

  public function presentaseKelulusan($trainingId)
  {
    $this->db->select('count(*) as jumlah_lulus');
    $this->db->where("training_id = $trainingId and status='lulus'");
    $this->db->from('training_karyawan');
    return $this->db->get()->result();;
  }

  // public function joinKaryawan($param1 = '*'){
  // 	$this->db->select("departemen.*, count(Employee_name) as 'jumlahKaryawan'"); 
  // 	$this->db->join('karyawan', 'departemen.nama = karyawan.Department', 'left')
  // 				->group_by('departemen.nama');

  //   return $this; 
  // }
}