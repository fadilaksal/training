<?php

class Departemen_model extends MY_Model {
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'departemen';
	}


  public function _select($param1 = '*'){ 
    $this->db->select($param1); 
    return $this; 
  } 
 
  public function _order_by($param1 = '', $param2 = ''){ 
    $this->db->order_by("$param1 $param2"); 
    return $this; 
  } 

  public function joinKaryawan($param1 = '*'){
  	$this->db->select("departemen.*, count(Employee_name) as 'jumlahKaryawan', (select count(*) from training_departemen where training_departemen.departemen_id = departemen.id) as 'jumlahTraining'"); 
  	$this->db->join('karyawan', 'departemen.nama = karyawan.department', 'left');
  	$this->db->group_by('departemen.nama');

    return $this; 
  }

  public function selectWithTrainingCount(){
    $this->db->select("nama as 'departemen', count(*) as 'jumlahTraining'");
    $this->db->join('training_departemen', 'departemen.id = training_departemen.departemen_id', 'left');
    $this->db->group_by('departemen');
    $this->db->order_by('jumlahTraining DESC');
    //$this->db->limit(10);
    return $this;
  }
}