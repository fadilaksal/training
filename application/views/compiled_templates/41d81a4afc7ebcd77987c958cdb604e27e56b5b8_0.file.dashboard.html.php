<?php
/* Smarty version 3.1.30, created on 2017-11-19 20:13:04
  from "E:\xampp\htdocs\training\application\views\main_templates\dashboard.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a118360407cb3_66788435',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41d81a4afc7ebcd77987c958cdb604e27e56b5b8' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\dashboard.html',
      1 => 1510536102,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a118360407cb3_66788435 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
  .btn-outline{
    border:1px solid blue;
  }
</style>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard Database Training</small>
      </h1>
    </section>
    <section class="content">
<!--Box -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Karyawan</span>
              <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['countKaryawan']->value;?>
</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-building-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Departemen</span>
              <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['countDepartemen']->value;?>
</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Training</span>
              <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['countTraining']->value;?>
</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Training tahun 2017</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div> -->

      <div class="row">
        <div class="col-es-12 col-sm-12 col-md-12 col-lg-12 connectedSortable">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-inbox"></i> Training tahun 2017</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>

            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <p class="text-center">
                    <strong>Training: 1 Jan, 2014 - 31 Dec, 2017</strong>
                  </p>

                  <div class="chart " id="revenue-chart" style="position: relative; height: 280px; padding:10px;">
                    <canvas id="barChart" style="min-height:260px; height: 270px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
              </div>
            </div>
            <div class="box-footer">
                <div class="col-sm-12 col-xs-12">
                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                      <h5 class="description-header text-green"><?php echo $_smarty_tpl->tpl_vars['countTraining']->value;?>
</h5>
                      <span class="description-text">Jumlah Tryout</span>
                    </div>
                  </div>

                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                      <h5 class="description-header text-red" id="totalBiaya"><?php echo $_smarty_tpl->tpl_vars['totalBiaya']->value;?>
</h5>
                      <span class="description-text">Total Biaya 1 Tahun</span>
                    </div>
                  </div>

                  <div class="col-sm-4 col-xs-6">
                    <div class="description-block border-right">
                      <a href="<?php echo base_url();?>
dashboard/export_training"><button type="button" class="btn btn-primary btn-outline"><i class="fa fa-file-excel-o"></i> Export</button></a>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-es-12 col-sm-12 col-md-12 col-lg-12 connectedSortable">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Tahun terakhir</a></li>
              
              <li class="pull-left header"><i class="fa fa-inbox"></i> Training</li>
            </ul>
            <div class="tab-content no-padding">
              
            </div>
            
          </div>
        </div> -->

        <div class="col-es-12 col-sm-12 col-md-12 col-lg-12 connectedSortable">
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#departemen-bar-chart" data-toggle="tab">Jumlah tahun terakhir</a></li>
              <!-- <li><a href="#departemen-pie-chart" data-toggle="tab">Pie</a></li> -->
              <li class="pull-left header"><i class="fa fa-inbox"></i> Departemen</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="departemen-bar-chart" style="position: relative; min-height: 280px; padding:5px;">
                <canvas id="barChart2" style="min-height:250px"></canvas>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-es-12 col-sm-12 col-md-6 col-lg-6 connectedSortable">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#karyawan-bar-chart" data-toggle="tab">Batang</a></li>
              <li><a href="#karyawan-pie-chart" data-toggle="tab">Pie</a></li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Karyawan</li>
            </ul>
            <div class="tab-content no-padding">
              <div class="chart tab-pane active" id="karyawan-bar-chart" style="position: relative; min-height: 280px; padding:10px;">
                <canvas id="barChart3" style="min-height:200px"></canvas>
              </div>
              <div class="chart tab-pane" id="karyawan-pie-chart" style="position: relative; height: 200px;">

              </div>
            </div>
          </div>
        </div> -->

        <div class="col-es-12 col-sm-12 col-md-6 col-lg-6">
        <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Training Terbaru</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Nama Training</th>
                    <th>Tanggal</th>
                    <th>Jenis</th>
                    <th>Jumlah Peserta</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lastFiveTraining']->value, 'value', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                  <?php $_smarty_tpl->_assignInScope('class', '');
?>
                  <?php if ($_smarty_tpl->tpl_vars['value']->value->jenis == 'bersertifikat') {?>
                    <?php $_smarty_tpl->_assignInScope('class', 'label-danger');
?>
                  <?php } elseif ($_smarty_tpl->tpl_vars['value']->value->jenis == 'teknikal') {?>
                    <?php $_smarty_tpl->_assignInScope('class', 'label-info');
?>
                  <?php } elseif ($_smarty_tpl->tpl_vars['value']->value->jenis == 'generic') {?>
                    <?php $_smarty_tpl->_assignInScope('class', 'label-success');
?>
                  <?php }?>
                  <tr>
                    <td><a href="<?php echo base_url();?>
training/id/<?php echo $_smarty_tpl->tpl_vars['value']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->nama;?>
</a></td>
                    <td><?php echo $_smarty_tpl->tpl_vars['value']->value->tanggal;?>
</td>
                    <td><span class="label <?php echo $_smarty_tpl->tpl_vars['class']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->jenis;?>
</span></td>
                    <td>20</td>
                  </tr>
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo base_url();?>
training/add" class="btn btn-sm btn-info btn-flat pull-left">Tambah Training</a>
              <a href="<?php echo base_url();?>
training" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua Training</a>
            </div>
            <!-- /.box-footer -->
          </div>
        </div>

      </div>
      </section><?php }
}
