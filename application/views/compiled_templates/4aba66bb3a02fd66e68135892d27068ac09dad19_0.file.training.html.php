<?php
/* Smarty version 3.1.30, created on 2017-11-19 20:09:42
  from "E:\xampp\htdocs\training\application\views\main_templates\training\training.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a11829691dc25_96330222',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4aba66bb3a02fd66e68135892d27068ac09dad19' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\training\\training.html',
      1 => 1510387231,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a11829691dc25_96330222 (Smarty_Internal_Template $_smarty_tpl) {
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="col-md-1 pull-right">
        <a href="<?php echo base_url();?>
training/export_all"><button class="btn btn-success"><i class="fa fa-download"></i> <strong> </strong>Excel</button></a>
      </div>
      <h1>
        Training
        <small>List Training Garuda Food</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Training</th>
                  <th>Tanggal Training</th>
                  <th>Jumlah Peserta</th>
                  <th>Jenis</th>
                </tr>
                </thead>
                <tbody>
                  <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value == null) {?>
                    <tr>
                      <td colspan="4" align="center"><h3>Data Training masih kosong</h3><a href="<?php echo base_url();?>
training/add">Tambah Training</a></td>
                    </tr>
                  <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dataTraining']->value, 'values', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['values']->value) {
?>
                    <tr>
                      <td><a href="<?php echo base_url();?>
training/id/<?php echo $_smarty_tpl->tpl_vars['values']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['values']->value->nama;?>
</a></td>
                      <td><?php echo $_smarty_tpl->tpl_vars['values']->value->tanggal;?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['values']->value->peserta;?>
</td>
                      <td>
                        <?php if ($_smarty_tpl->tpl_vars['values']->value->jenis == 'basic') {
$_smarty_tpl->_assignInScope('label', 'label-primary');
?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['values']->value->jenis == 'generic') {
$_smarty_tpl->_assignInScope('label', 'label-info');
?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['values']->value->jenis == 'teknikal') {
$_smarty_tpl->_assignInScope('label', 'label-success');
?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['values']->value->jenis == 'bersertifikat') {
$_smarty_tpl->_assignInScope('label', 'label-danger');
?>
                        <?php }?>
                        <label class="label <?php echo $_smarty_tpl->tpl_vars['label']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['values']->value->jenis;?>
</label>
                      </td>
                    </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Nama Training</th>
                  <th>Tanggal Training</th>
                  <th>Jumlah Peserta</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content --><?php }
}
