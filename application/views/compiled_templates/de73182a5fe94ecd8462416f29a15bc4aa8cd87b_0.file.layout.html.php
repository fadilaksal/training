<?php
/* Smarty version 3.1.30, created on 2017-11-19 20:09:42
  from "E:\xampp\htdocs\training\application\views\main_templates\layout.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a118296d6c6f6_61536252',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'de73182a5fe94ecd8462416f29a15bc4aa8cd87b' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\layout.html',
      1 => 1510539368,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a118296d6c6f6_61536252 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Database Trainer</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo assets_url();?>
bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo assets_url();?>
dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
dist/css/style.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
dist/css/dropdown-multilevel.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
dist/css/uploadskin.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo assets_url();?>
dist/css/skins/skin-blue.min.css">

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.2.3 -->
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
plugins/jQuery/jquery-2.2.3.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"><?php echo '</script'; ?>
>
  <!-- Bootstrap 3.3.6 -->
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
  <!-- ChartJS 1.0.1 -->
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
plugins/chartjs/Chart.js"><?php echo '</script'; ?>
>
  <!-- <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"><?php echo '</script'; ?>
> -->
  <!-- AdminLTE App -->
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
dist/js/app.min.js"><?php echo '</script'; ?>
>
  <!-- DataTables -->
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
plugins/datatables/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
plugins/datatables/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
plugins/datepicker/bootstrap-datepicker.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
dist/js/custom-file-input.js"><?php echo '</script'; ?>
>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="" class="logo">
      <span class="logo-mini"><b>T</b>I</span>
      <span class="logo-lg"><b>Training</b>Information</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="nav-title">Database Training</div>
    </nav>
  </header>
<?php $_smarty_tpl->_assignInScope('dashboard', '');
if (isset($_smarty_tpl->tpl_vars['page']->value)) {?>
  <?php if ($_smarty_tpl->tpl_vars['page']->value == 'Dashboard') {?>
    <?php $_smarty_tpl->_assignInScope('dashboard', 'active');
?>
  <?php }
}?>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php echo $_smarty_tpl->tpl_vars['dashboard']->value;?>
">
          <a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li class="treeview <?php echo $_smarty_tpl->tpl_vars['training_menu']->value;?>
">
          <a href="#"><i class="fa fa-file-text-o"></i> <span>Training</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php echo $_smarty_tpl->tpl_vars['training_menu']->value;?>
">
            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'list') {?>class='active'<?php }?>><a href="#">
                <i class="fa fa-circle-o"></i> List Training
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'all') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training"><i class="fa fa-circle-o"></i> Semua Training</a>
                </li>
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'basic') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training/jenis/basic"><i class="fa fa-circle-o"></i> Basic</a>
                </li>
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'generic') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training/jenis/generic"><i class="fa fa-circle-o"></i> Generik</a>
                </li>
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'teknikal') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training/jenis/teknikal"><i class="fa fa-circle-o"></i> Spesifik/Teknikal</a>
                </li>
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'bersertifikat') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training/jenis/bersertifikat"><i class="fa fa-circle-o"></i> Bersertifikat</a>
                </li>
              </ul>
            </li>

            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'add') {?>class='active'<?php }?>>
              <a href="<?php echo base_url();?>
training/add">
                <i class="fa fa-circle-o"></i> Tambah Training
                <!-- <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span> -->
              </a>
              <!-- <ul class="treeview-menu">
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'new') {?>class='active'<?php }?>>
                  <a href="<?php echo base_url();?>
training/add"><i class="fa fa-circle-o"></i>Tambah Baru</a>
                </li>
                <li <?php if ($_smarty_tpl->tpl_vars['category']->value == 'karyawan') {?>class='active'<?php }?>> 
                  <a href="<?php echo base_url();?>
training/add_karyawan"><i class="fa fa-circle-o"></i> Masukkan data Karyawan</a>
                </li>
              </ul> -->
            </li>
            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'edit') {?>class='active'<?php }?>>
              <a href="<?php echo base_url();?>
training/edit"><i class="fa fa-circle-o"></i>Edit Training</a>
            </li>
            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'absensi') {?>class='active'<?php }?>>
              <a href="<?php echo base_url();?>
training/absen"><i class="fa fa-circle-o"></i>Absensi Training</a>
            </li>
          </ul>
        </li>
        <li class="treeview <?php echo $_smarty_tpl->tpl_vars['departemen_menu']->value;?>
">
          <a href="#">
            <i class="fa fa-building-o"></i> <span>Departemen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'list') {?>class='active'<?php }?>>
              <a href="<?php echo base_url();?>
departemen"><i class="fa fa-circle-o"></i>List Departemen</a>
            </li> 
            <li <?php if ($_smarty_tpl->tpl_vars['module']->value == 'add') {?>class='active'<?php }?>>
              <a href="<?php echo base_url();?>
departemen/add"><i class="fa fa-circle-o"></i>Tambah Departemen</a>
            </li> 
          </ul> 
        </li>
        <li class="treeview <?php echo $_smarty_tpl->tpl_vars['karyawan_menu']->value;?>
">
          <a href="#">
            <i class="fa fa-users"></i> <span>Karyawan</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>
karyawan"><i class="fa fa-circle-o"></i>List data karyawan</a></li>
            <li><a href="<?php echo base_url();?>
karyawan/upload"><i class="fa fa-circle-o"></i>Upload data karyawan</a></li>
          </ul>
        </li>
        
        <li class="header">ADMINISTRATION</li>
        <li <?php if ($_smarty_tpl->tpl_vars['page']->value == 'Admin') {?>class='active'<?php }?>>
          <a href="<?php echo base_url();?>
admin"><i class="fa fa-key"></i> <span>Account</span></a>
        </li>
        <li>
          <a href="<?php echo base_url();?>
auth/logout"><i class="fa fa-arrow-circle-left"></i> <span>Log Out</span></a>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <section class="content-header module-head">
        <ul class="module">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Training <span class="caret"></span></a>
            <ul class="dropdown-menu first" role="menu">
                <li class="dropdown-submenu">
                  <a href="#">List Training</a>
                  <ol class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>
training"><i class="fa fa-circle-o"></i>Semua</a></li>
                    <li><a href="<?php echo base_url();?>
training/jenis/basic"><i class="fa fa-circle-o"></i>Basic</a></li>
                    <li><a href="<?php echo base_url();?>
training/jenis/generic"><i class="fa fa-circle-o"></i>Generik</a></li>
                    <li><a href="<?php echo base_url();?>
training/jenis/teknikal"><i class="fa fa-circle-o"></i>Spesifik/Teknikal</a></li>
                    <li><a href="<?php echo base_url();?>
training/jenis/bersertifikat"><i class="fa fa-circle-o"></i>Bersertifikat</a></li>
                  </ol>
                </li>
                <li class="dropdown-submenu">
                  <a href="#">Departemen</a>
                  <ul class="dropdown-menu">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['departemenList']->value, 'value', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <li><a href="<?php echo base_url();?>
departemen/id/<?php echo $_smarty_tpl->tpl_vars['value']->value->id;?>
"><i class="fa fa-circle-o"></i><?php echo $_smarty_tpl->tpl_vars['value']->value->nama;?>
</a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </ul>
                </li>
                <li class="dropdown-submenu">
                  <a href="#">Tambah Training</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>
training/add"><i class="fa fa-circle-o"></i>Tambah Baru</a></li>
                    <li><a href="<?php echo base_url();?>
training/add_karyawan"><i class="fa fa-circle-o"></i>Masukkan Karyawan</a></li>
                  </ul>
                </li>
                <li><a href="<?php echo base_url();?>
training/edit">Ubah Training</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Departemen <span class="caret"></span></a>
            <ul class="dropdown-menu first" role="menu">
                <li class="dropdown-submenu">
                  <a href="#">List Departemen</a>
                  <ol class="dropdown-menu">
                    <li><a href="<?php echo base_url();?>
departemen">List Semua Departemen</a></li>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['departemenList']->value, 'value', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <li><a href="<?php echo base_url();?>
departemen/id/<?php echo rawurlencode($_smarty_tpl->tpl_vars['value']->value->id);?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->nama;?>
</a></li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </ol>
                </li>
              </ul>
          </li>
          <li class="dropdown">
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Karyawan <span class="caret"></span></a>
            <ul class="dropdown-menu first" role="menu">
                <li>
                  <a href="<?php echo base_url();?>
karyawan">List Data Karyawan</a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>
karyawan/upload">Upload Data Karyawan</a>
                </li>
              </ul>
          </li>
        </ul>
    </section>

    <!-- Main content -->
    
      <?php if (isset($_smarty_tpl->tpl_vars['main_content']->value)) {?>
        <?php echo $_smarty_tpl->tpl_vars['main_content']->value;?>

      <?php }?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->
<?php echo '<script'; ?>
 type="text/javascript">
 $(function () {
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu .first', this).not('.in .dropdown-menu .first').stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu .first', this).not('.in .dropdown-menu .first').stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
    
    $("#table1").DataTable();
    // console.log(<?php echo $_smarty_tpl->tpl_vars['dataJS']->value;?>
);
    var dataJS = '<?php echo $_smarty_tpl->tpl_vars['dataToJS']->value;?>
';

    <?php if (isset($_smarty_tpl->tpl_vars['js_script']->value)) {?> //untuk menyisipkan script dari url
      var s = document.createElement( 'script' );
      var src = '<?php echo assets_url();?>
dist/js/<?php echo $_smarty_tpl->tpl_vars['js_script']->value;?>
';
      s.dataset.dataJS = dataJS;
      s.setAttribute( 'src', src);
      document.body.appendChild( s );
    <?php }?>

  });
<?php echo '</script'; ?>
>
<?php if (isset($_smarty_tpl->tpl_vars['js_include']->value)) {?>
  <?php echo '<script'; ?>
 src="<?php echo assets_url();?>
dist/js/<?php echo $_smarty_tpl->tpl_vars['js_include']->value;?>
?asd"><?php echo '</script'; ?>
>
<?php }?>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
<?php }
}
