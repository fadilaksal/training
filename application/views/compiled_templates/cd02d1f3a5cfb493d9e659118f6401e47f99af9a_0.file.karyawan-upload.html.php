<?php
/* Smarty version 3.1.30, created on 2017-11-20 10:57:25
  from "E:\xampp\htdocs\training\application\views\main_templates\karyawan\karyawan-upload.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a1252a52b5fb0_14362971',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd02d1f3a5cfb493d9e659118f6401e47f99af9a' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\karyawan\\karyawan-upload.html',
      1 => 1510550740,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a1252a52b5fb0_14362971 (Smarty_Internal_Template $_smarty_tpl) {
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url();?>
/karyawan">Karyawan</a></li>
        <li><a href="#">Upload data</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-8 col-md-8 col-xs-8" style="margin: 0 auto; float: none;">
          <div class="box box-success" style="padding: 10px;">
            <div class="box-body">
              <?php if (isset($_smarty_tpl->tpl_vars['isSuccess']->value) && $_smarty_tpl->tpl_vars['isSuccess']->value != null) {?>
                <?php if ($_smarty_tpl->tpl_vars['isSuccess']->value == true) {?>
                <div class="row">
                  <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><strong>x</strong></button>
                  Berhasil mengupload Data Karyawan</div>
                </div>
                <?php } else { ?>
                <div class="row">
                  <div class="alert alert-Danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><strong>x</strong></button>
                  Gagal</div>
                </div>
                <?php }?>
              <?php }?>
              <form role="form" id="formInput" action="<?php echo base_url();?>
karyawan/proses_upload" method="post" enctype="multipart/form-data">
                <div class="form-group col-md-12" style="text-align: center;">
                  <div style="font-size:20pt; color:green; padding:10px 10px 20px 10px;">
                    <label  for="uploadKaryawan">Upload Data Karyawan</label>
                  </div>
                  <div class="row">
                    <!-- <input type="file" name="dataKaryawan" id="file-7" class="inputfile inputfile-6" data-multiple-caption=" {count}  files selected" style="height: 0.01px; width: 0.01px;" multiple /> -->
                    <!-- <label for="file-7"><span></span> <strong> Klik untuk memilih file&hellip;</strong></label> -->
                    <label  for="InputJenis" class="control-label col-md-4">Pilih File</label>
                    <div class="col-md-7">
                    <input type="file" name="dataKaryawan" accept=".csv" class="inputfile"><br>
                    <span class="help-block" style="text-align: left;">File harus bertipe .csv</span>
                  </div>
                  <div class="row">
                    <button type="submit" class="btn btn-primary" id="buttonSubmit" style="width:520px;">
                      <div class="col-md-1" style="float: right;" id="loader"></div>
                      <i class="fa fa-upload" id="iconUpload"></i> Upload
                    </button>
                  </div>
                  <div class="row">
                    <div class="col-md-10" style="text-align: left; margin: 0 auto; float: none;">
                      <span class="help-block" style="color:red;" id="errorText"></span>
                    </div>
                  </div>
                </div>
              </form>
            <!-- /.box-body -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content --><?php }
}
