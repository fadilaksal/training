<?php
/* Smarty version 3.1.30, created on 2017-11-21 09:35:14
  from "E:\xampp\htdocs\training\application\views\main_templates\karyawan\karyawan.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a1390e2e39818_57242952',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6dcda4dcd99dd083eeb239b02395361dbaf79d4a' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\karyawan\\karyawan.html',
      1 => 1510539302,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a1390e2e39818_57242952 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="content-header">
  <h1>
    Karyawan
    <small>List Karyawan Garuda Food</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Karyawan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- <div class="box-header">
          <h3 class="box-title">List Karyawan</h3>
        </div> -->
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table2" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Reg</th>
              <th>Nama</th>
              <th>Departemen</th>
              <!-- <th>Action</th> -->
            </tr>
            </thead>
            <tbody>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dataKaryawan']->value, 'value', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
              <tr>
                <td><a href="<?php echo base_url();?>
karyawan/id/<?php echo $_smarty_tpl->tpl_vars['value']->value->employee_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->employee_ID;?>
</a></td>
                <td><?php echo $_smarty_tpl->tpl_vars['value']->value->employee_name;?>
</td>
                <!-- <td>Bagian A</td> -->
                <td><?php echo $_smarty_tpl->tpl_vars['value']->value->department;?>
</td>
                <!-- <td>
                  <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                  <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</button>
                </td> -->
              </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </tbody>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Nama</th>
              <th>Departemen</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<!-- /.content --> 
<?php echo '<script'; ?>
 type="text/javascript">
  $('#table2').DataTable();
<?php echo '</script'; ?>
><?php }
}
