<?php
/* Smarty version 3.1.30, created on 2017-11-21 08:57:53
  from "E:\xampp\htdocs\training\application\views\main_templates\training\training-departemen.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5a138821974302_32163742',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a84c0db494a79425ea33c6bb18a8f708b2c0f3d' => 
    array (
      0 => 'E:\\xampp\\htdocs\\training\\application\\views\\main_templates\\training\\training-departemen.html',
      1 => 1511229463,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a138821974302_32163742 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
  h4{
    margin: 0;
    padding: 0;
  }
  
  .box-info{
    font-size: 12px;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->nama;?>
 - <?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->departemen;?>

  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo base_url();?>
training/id/<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->nama;?>
</a></li>
    <li><a href="#"><?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->departemen;?>
</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text box-info">Jumlah Peserta</span>
          <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->peserta;?>
</span>
        </div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-check"></i></span>

        <div class="info-box-content">
          <span class="info-box-text box-info">Persen Kelulusan</span>
          <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['lulus']->value;?>
 %</span>
        </div>
      </div>
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-smile-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text box-info">Persen Kepuasan</span>
          <span class="info-box-number"><?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->persen_kepuasan;?>
 %</span>
        </div>
        <div class="info-box-footer" style="float:right; margin-right: 5px;">
          <button type="button" class="btn btn-primary" id="openEditKepuasan" data-toggle="modal" data-target="#editKepuasan" >Edit</button>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box" style="padding:10px;">
        <div class="box-header">
          <h4>Karyawan yang mengikuti Training</h4>
          
          <a href="<?php echo base_url();?>
training/exportTraining/<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
/<?php echo rawurlencode($_smarty_tpl->tpl_vars['dataTraining']->value->departemen);?>
"><button class="btn btn-success btn-sm pull-right"  style="margin-left: 20px;"><span class="fa fa-file-o"></span> Export</button></a>
          <button type="button" data-toggle="modal" data-target="#uploadNilaiDialog" data-id="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->nama;?>
" title="Edit this item" class="openDialog btn btn-primary btn-sm pull-right">
            <i class="fa fa-upload"></i> Upload Nilai
          </button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th> </th>
                <th>Reg</th>
                <th>Nama</th>
                <th>Line</th>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan != 0) {?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training') {?><th>Nilai Pre-Test</th><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><th>Nilai Post-Test</th><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><th>Status</th><?php }?>
                <?php }?>
                <th>Manhour</th>
              </tr>
            </thead>
            <tbody>
              <?php $_smarty_tpl->_assignInScope('manhour', 0);
?>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dataKaryawan']->value, 'value', false, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
              <?php $_smarty_tpl->_assignInScope('style', '');
?>
              <?php if ($_smarty_tpl->tpl_vars['value']->value->pre_test < $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan && $_smarty_tpl->tpl_vars['value']->value->post_test == 0) {?>
                <?php $_smarty_tpl->_assignInScope('style', 'color:#CF2525; font-weight: bold;');
?>
              <?php } elseif ($_smarty_tpl->tpl_vars['value']->value->pre_test >= $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan && $_smarty_tpl->tpl_vars['value']->value->post_test == 0) {?>
                <?php $_smarty_tpl->_assignInScope('style', 'color:green; font-weight: bold;');
?>
              <?php } elseif ($_smarty_tpl->tpl_vars['value']->value->post_test < $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan) {?>
                <?php $_smarty_tpl->_assignInScope('style', 'color:#CF2525; font-weight: bold;');
?>
              <?php } elseif ($_smarty_tpl->tpl_vars['value']->value->post_test >= $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan) {?>
                <?php $_smarty_tpl->_assignInScope('style', 'color:green; font-weight: bold;');
?>
              <?php }?>
              <tr style="<?php echo $_smarty_tpl->tpl_vars['style']->value;?>
">
                <td>
                  <button type="button" data-toggle="modal" data-target="#addBookDialog" data-id="<?php echo $_smarty_tpl->tpl_vars['value']->value->id;?>
" title="Edit this item" class="openDialog btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>

                </td>
                
                <td class="employee_ID"><a href="<?php echo base_url();?>
karyawan/id/<?php echo $_smarty_tpl->tpl_vars['value']->value->employee_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->employee_ID;?>
</a></td>
                <td class="employee_name"><?php echo $_smarty_tpl->tpl_vars['value']->value->employee_name;?>
</td>
                <td class="line"><?php echo $_smarty_tpl->tpl_vars['value']->value->line;?>
</td>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan != 0) {?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training') {?><td class="pretest"><?php echo $_smarty_tpl->tpl_vars['value']->value->pre_test;?>
</td><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><td class="posttest"><?php echo $_smarty_tpl->tpl_vars['value']->value->post_test;?>
</td><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><td class="status"><?php echo $_smarty_tpl->tpl_vars['value']->value->status;?>
</td><?php }?>
                <?php }?>
                <td><?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->durasi;?>
</td>
              </tr>
              <?php $_smarty_tpl->_assignInScope('manhour', $_smarty_tpl->tpl_vars['manhour']->value+$_smarty_tpl->tpl_vars['dataTraining']->value->durasi);
?>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </tbody>
            <tfoot>
              <tr>
                <th> </th>
                <th>Reg</th>
                <th>Nama</th>
                <th>Line</th>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan != 0) {?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training') {?><th>Nilai Pre-Test</th><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><th>Nilai Post-Test</th><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?><th>Status</th><?php }?>
                <?php }?>
                <th>Manhour</th>
              </tr>
            </tfoot>
          </table>
          <b>Total Manhour : <?php echo $_smarty_tpl->tpl_vars['manhour']->value;?>
<br>
          <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') {?>
          Batas Nilai Kelulusan : <?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan;?>
</b>
          <?php }?>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>

  <?php if (($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training' || $_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'refresh') && $_smarty_tpl->tpl_vars['dataTraining']->value->nilai_kelulusan != 0) {?>
  <div class="row">
    <!-- <div class="col-md-6">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Statistik Kehadiran</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="barChart" style="height:400px"></canvas>
          </div>
        </div>
      </div>
    </div> -->
    <div class="col-md-6">
      <div class="box box-success" style="background:rgb(245,255,245);">
        <div class="box-header with-border">
          <h3 class="box-title">Statistik Kelulusan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="barChart2" style="height:400px"></canvas>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary" style="background:rgb(245,255,245);">
        <div class="box-header with-border">
          <h3 class="box-title">Nilai Karyawan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="barChart3" style="height:400px"></canvas>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
  <?php }?>

  <div class="modal fade" id="addBookDialog" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" data-dismiss="modal">×</button>
          <h3>Edit Training Karyawan</h3>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="<?php echo base_url();?>
training/edit_training_karyawan" id="formEdit" method="post">
                <input type="hidden" name="id" class="form-control" id="id" placeholder="Enter ID">
                <input type="hidden" name="idTraining" value="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
">
                <div class="form-group">
                  <label  for="InputEmployeeID" class="control-label col-md-3">Employee ID</label>
                  <div class="col-md-8">
                    <input type="text" name="employeeID" class="form-control" id="InputEmployeeID" required placeholder="Employee ID" disabled="true">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="InputName" class="control-label col-md-3">Nama Karyawan</label>
                  <div class="col-md-8">
                    <input type="text" name="nama" class="form-control" id="InputName" required placeholder="Nama Karyawan" disabled="true">
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label  for="InputKehadiran" class="control-label col-md-3">Kehadiran</label>
                  <div class="col-md-8">
                    <select class="form-control" name="kehadiran" id="InputKehadiran" required>
                      <option value="tidak">Tidak Hadir</option>
                      <option value="hadir">Hadir</option>
                    </select>
                  </div>
                </div> -->
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori == 'training') {?>
                <div class="form-group">
                  <label for="InputPreTest" class="control-label col-md-3">Nilai Pre Test</label>
                  <div class="col-md-8">
                  <input type="number" name="pretest" class="form-control" id="InputPreTest" required placeholder="Nilai Pre Test">
                  </div>
                </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['dataTraining']->value->kategori != 'sosialisasi') {?>
                <div class="form-group">
                  <label for="InputPostTest" class="control-label col-md-3">Nilai Post Test</label>
                  <div class="col-md-8">
                  <input type="number" name="posttest" class="form-control" id="InputPostTest" required placeholder="Nilai Post Test">
                  </div>
                </div> 
                <?php }?>
                <!-- <div class="form-group">
                  <label  for="InputStatus" class="control-label col-md-3">Status</label>
                  <div class="col-md-8">
                    <select class="form-control" name="status" id="InputStatus" required>
                      <option value="remidi">Remidi</option>
                      <option value="lulus">Lulus</option>
                    </select>
                  </div>
                </div> -->
                <div class="form-group"> 
                  <label class="col-md-3"> </label>
                  <div class="col-md-8">
                    <button type="submit" class="btn btn-primary">Edit</button>
                  </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div class="modal fade" id="uploadNilaiDialog" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <form role="form" id="formInput" action="<?php echo base_url();?>
training/upload_nilai" method="post" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header" style="text-align: center;">
          <button class="close" data-dismiss="modal">×</button>
          <label style="font-size:20pt; color:green;" for="uploadKaryawan">Upload Absensi Karyawan</label>
        </div>
        <div class="modal-body">
          <div class="form-group col-md-12" style="text-align: center;">
            <div class="row" style="float: none; margin: 20px auto;">
              <label  for="InputJenis" class="control-label col-md-4">Nama Training</label>
              <div class="col-md-7 col-sm-7 col-xs-7">
                <input type="hidden" class="form-control" name="training_id" value="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
">
                <input type="text" class="form-control" name="training_name" disabled="true" value="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->nama;?>
">
              </div>
            </div>
            <div class="row">
              <!-- <input type="file" name="dataNilai" id="file-7" class="inputfile inputfile-6" data-multiple-caption=" {count}  files selected" style="height: 0.01px; width: 0.01px;" multiple /> -->
              <label  for="InputJenis" class="control-label col-md-4">Pilih File</label>
              <div class="col-md-7">
              <input type="file" name="dataNilai" accept=".csv" class="inputfile"><br>
              <span class="help-block" style="text-align: left;">File harus bertipe .csv</span>
              <!-- <label for="file-7" style="width: 420px;"><span></span> <strong> Pilih File</strong></label> -->
              <!-- <span class="help-block">File harus bertipe .csv</span> -->
            </div>
            <div class="row">
              
            </div>
            <div class="row">
              <div class="col-md-10" style="text-align: left; margin: 0 auto; float: none;">
                <span class="help-block" style="color:red;" id="errorText"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          <button type="submit" class="btn btn-primary" id="buttonSubmit" style="width:520px;">
            <div class="col-md-1" style="float: right;" id="loader"></div>
            <i class="fa fa-upload" id="iconUpload"></i> Upload
          </button>
        </div>
      </div>
      </form>
    </div>
  </div>

  <div class="modal fade" id="editKepuasan" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <form role="form" id="formUbahPersenKepuasan" action="" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header" style="text-align: center;">
            <button class="close" data-dismiss="modal">×</button>
            <label style="font-size:14pt;" for="uploadKaryawan">Edit Persen Kepuasan</label>
          </div>
          <div class="modal-body">
            <div class="row" style="float: none; margin: 20px auto;">
              <label  for="inputKepuasan" class="control-label col-md-4">Persen Kepuasan</label>
              <div class="col-md-7 col-sm-7 col-xs-7">
                <input type="hidden" class="form-control" name="training_id" value="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->id;?>
">
                <input type="number" class="form-control" name="persen_kepuasan" id="inputKepuasan" value="<?php echo $_smarty_tpl->tpl_vars['dataTraining']->value->persen_kepuasan;?>
" >
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="submit"><i class="fa fa-edit"></i> Edit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- /.content -->

<?php echo '<script'; ?>
 type="text/javascript">
  var dataJSON            = '<?php echo $_smarty_tpl->tpl_vars['dataToJS']->value;?>
';
  var ctx                = document.getElementById('barChart2').getContext('2d');
  var ctx2                = document.getElementById('barChart3').getContext('2d');
  var dataJSON            = JSON.parse(dataJSON);
  console.log(dataJSON);
  var dataKaryawan = dataJSON.dataKaryawan;
  var namaKaryawan = [];
  var nilaiKaryawan = [];
  let i = 0;
  for (var key in dataKaryawan) {
    namaKaryawan[i] = dataKaryawan[key].employee_name;
    // console.log(dataKaryawan[key][employee_name]);
    nilaiKaryawan[i] = dataKaryawan[key].post_test;
    i++;
  }

  var statistikHadir      = dataJSON.statistikHadir;
  var statistikKelulusan  = dataJSON.statistikKelulusan;
  var tidakHadir          = (statistikHadir.tidakHadir / statistikHadir.jumlah * 100).toFixed(2);
  var hadir               = (statistikHadir.hadir / statistikHadir.jumlah * 100).toFixed(2);
  var dataHadir           = [hadir, tidakHadir];
  var labelHadir          = ['Hadir', 'Tidak Hadir'];

  var lulus = (statistikKelulusan.lulus / statistikKelulusan.jumlah * 100).toFixed(2);
  var tidak = (statistikKelulusan.tidak / statistikKelulusan.jumlah * 100).toFixed(2);

  var dataLulus   = [lulus, tidak];
  var labelLulus  = ['Lulus', 'Tidak Lulus'];

  var options = {
    hover: {  animationDuration: 0  }, 
    scales: {
      xAxes:[{
          gridLines: {  display:false }
      }],
      yAxes:[{
        ticks: {
            suggestedMin: 0,
            suggestedMax: 100,
            stepSize:20
        },
        stacked:true
      }]
    },
    animation: {
      duration: 1,
      onComplete: function () {
        var chartInstance = this.chart,
            ctx = chartInstance.ctx;
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
        ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];                            
                ctx.fillText(data + "%", bar._model.x, bar._model.y - 5);
            });
        });
      }
    },
    responsive:true
  };

  var options2 = {
    lineAt: dataJSON.dataTraining.nilai_kelulusan,
    hover: {  animationDuration: 0  }, 
    scales: {
      xAxes:[{
          gridLines: {  display:false },
          ticks: {
            stepSize: 1,
            min: 0,
            autoSkip: false
          }
      }],
      yAxes:[{
        ticks: {
            suggestedMin: 0,
            suggestedMax: 100,
            stepSize:10
        },
        gridLines: {
          display:false
        },
        stacked:true
      }]
    },
    animation: {
      duration: 1,
      onComplete: function () {
        var chartInstance = this.chart,
            ctx = chartInstance.ctx;
        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
        ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];                            
                ctx.fillText(data , bar._model.x, bar._model.y - 5);
            });
        });
      }
    },
    responsive:true
  };

  var dataOption = {
    type: 'bar',
    data: {
         labels: labelLulus,
          datasets: [
            {
              label: "Presentasi kelulusan",
              backgroundColor: [
                'rgba(0, 180, 99, 1)',
                'rgba(255,88,132,1)'
              ],
              borderColor: [
                'rgba(0, 132, 99, 0.6)',
                'rgba(255,20,132, 0.6)'],
              borderWidth: 2,
              data: dataLulus
            }
          ]
    },
    options : options
  };

  var dataOption2 = {
    type: 'bar',
    data: {
         labels: namaKaryawan,
          datasets: [
            {
              label: "Nilai Karyawan",
              backgroundColor: [
                'rgba(0, 180, 99, 1)',
                'rgba(255,88,132,1)'
              ],
              borderColor: [
                'rgba(0, 132, 99, 0.6)',
                'rgba(255,20,132, 0.6)'],
              borderWidth: 2,
              data: nilaiKaryawan
            }
          ]
    },
    options : options2
  };
  
  Chart.pluginService.register({
    afterDraw: function(chart) {
      // console.log(chart);
        if (typeof chart.config.options.lineAt != 'undefined') {
          var lineAt = chart.config.options.lineAt;
            var ctxPlugin = chart.chart.ctx;
            var xAxe = chart.scales[chart.config.options.scales.xAxes[0].id];
            var yAxe = chart.scales[chart.config.options.scales.yAxes[0].id];
            
            // I'm not good at maths
            // So I couldn't find a way to make it work ...
            // ... without having the `min` property set to 0
            // console.log(yAxe.min);
            if(yAxe.min != 0) return;
            
            ctxPlugin.strokeStyle = "red";
            ctxPlugin.beginPath();
            lineAt = (lineAt - yAxe.min) * (100 / yAxe.max);
            lineAt = (100 - lineAt) / 100 * (yAxe.height) + yAxe.top;
            ctxPlugin.moveTo(xAxe.left, lineAt);
            ctxPlugin.lineTo(xAxe.right, lineAt);
            ctxPlugin.stroke();
        }
    }
  });

  var chart = new Chart(ctx, dataOption);
  var chart2 = new Chart(ctx2, dataOption2);

  $('#barChart').click(
      function(evt){
         var activePoints = chart.getElementsAtEvent(evt);
         var firstPoint = activePoints[0];
          if (firstPoint !== undefined){
            location.href="training-departemen.html";
          }
      }
    );

  $('.openDialog').click(
    function (evt) {
      var id = $(this).attr("data-id");
      var nama = $(this).closest('tr').children('td.employee_name').text();
      var employee_ID = $(this).closest('tr').children('td.employee_ID').text();
      var kehadiran = $(this).closest('tr').children('td.kehadiran').text();
      var pretest = $(this).closest('tr').children('td.pretest').text();
      var posttest = $(this).closest('tr').children('td.posttest').text();
      var status = $(this).closest('tr').children('td.status').text();
      
      $("input#id").val( id );
      $("input#InputName").val( nama );
      $("input#InputEmployeeID").val( employee_ID );
      $("input#InputKehadiran").val( kehadiran );
      $("input#InputPreTest").val( pretest );
      $("input#InputPostTest").val( posttest );
      $("input#InputStatus").val( status );
      //console.log(id + nama);
    });

  $('#formUbahPersenKepuasan').submit(function(e){
    var url = "http://localhost/training/training/editPersenKepuasan"; 
    $.ajax({
     type: "POST",
     url: url,
     data: $("#formUbahPersenKepuasan").serialize(), 
     success: function(data)
     {
        if(data == 'true'){
          swal('Sukses', 'Berhasil mengubah persen kepuasan', 'success').then(function(){
            location.reload();
          }); 
        }else{
          swal('Gagal', 'Gagal mengubah persen kepuasan', 'error');
        }
      },
      error: function(xhr, textStatus, errorThrown){
        swal('Gagal', 'Gagal mengubah persen kepuasan', 'error');
      }
    });
    e.preventDefault(); // avoid to execute the actual submit of the form.
  });

  $('#formEdit').submit(function(e) {
    var url = "http://localhost/training/training/proses_edit_training_karyawan"; 
    $.ajax({
     type: "POST",
     url: url,
     data: $("#formEdit").serialize(), 
     success: function(data)
     {
        if(data == 'true'){
          swal('Sukses', 'Berhasil mengubah Karyawan', 'success').then(function(){ 
            location.reload();  
          }); 
        }else{
          swal('Gagal', 'Gagal mengubah Karyawan', 'error');
        }
      },
      error: function(xhr, textStatus, errorThrown){
        swal('Gagal', 'Gagal mengubah Karyawan', 'error');
      }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
  });

<?php echo '</script'; ?>
><?php }
}
