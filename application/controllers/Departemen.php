<?php

class Departemen extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->auth->logged_in()) {
            // redirect them to the login page
            redirect('auth', 'refresh');
        } 

        $this->data["departemen_menu"] = "active";
        $this->data['page'] = "Departemen";
    }

    public function index() {
        $this->data['module'] = "list";
        $dataDepartemen = $this->departemen_m->joinKaryawan()->_order_by('id', 'ASC')->get_all();

        $this->data['dataDepartemen'] = $dataDepartemen;
        $this->data["main_content"] = $this->smarty->view("departemen/departemen.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function id($param1 = 0){
        if((int)$param1 <= 0){
            redirect(base_url() . 'dashboard');
        }

        $dataDepartemen = $this->departemen_m->get($param1);
        $dataDepartemen->jumlahKaryawan = $this->karyawan_m->count_by('Department', $dataDepartemen->nama);

        $this->data['page']         = "Departemen";
        // $this->data['js_script']    = 'hapus_departemen.js';
        $this->data['dataTraining'] = $this->training_departemen_m->_joinTraining()->get_many_by(array('departemen_id' => $param1));
        $this->data['dataDepartemen'] = $dataDepartemen;
        
        $this->data["main_content"] = $this->smarty->view("departemen/departemen-id.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function add(){
        //$this->data['page'] = 'Tambah Departemen';
        $this->data['module'] = 'add';
        // $this->data['js_script'] = 'inputDate.js';
        $this->data["main_content"] = $this->smarty->view("departemen/departemen-add.html", $this->data, true);
        // $this->data['js_include'] = 'ajax_add_departemen.js';
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function edit($param1 = 0){

        if($param1 == 0){
            redirect(base_url() . 'dashboard');
        }
        $data = $this->departemen_m->get($param1);
        //die(var_dump($data));
        
        //$data->tanggal = date("d/m/Y", strtotime($data->tanggal)); 
        $this->data['dataDepartemen'] = $data;
        $this->data['page'] = 'Tambah Departemen';
        $this->data['js_script'] = 'inputDate.js';
        $this->data["main_content"] = $this->smarty->view("departemen/departemen-edit-id.html", $this->data, true);
        $this->data['js_include'] = 'ajax_edit_departemen.js';
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_add(){
        $this->load->model('departemen_model');
        $data = $this->input->post();
        $insertId = $this->departemen_model->insert($data);
        if($insertId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function proses_edit(){
        $data = $this->input->post();
        //$tanggal = date("Y-m-d", strtotime($data['tanggal'])); 
        //$data['tanggal'] = $tanggal;
        $updateId = $this->departemen_m->update($data['id'], $data);
        if($updateId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function hapus($param1 = ''){
        $id = $this->input->post('id');
        $deleteId = $this->departemen_m->delete($id, TRUE);
        if($deleteId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

}
