<?php

class Configs extends MY_Controller {
	function __construct()
	{
		parent::__construct();
                if (!$this->auth->logged_in()) {
                    redirect('auth');
                }
                $this->data['page'] = "Setting";
	}

	public function index()
	{
            $crud = new Grocery_CRUD();
            $crud->unset_jquery();
            $crud->set_table('configs');
            $crud->set_subject('Setting');
            $crud->unset_list();
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_back_to_list();
            $crud->unset_read();
            if ($crud->getState() == "edit") {
            $id = $crud->getStateInfo()->primary_key;
            switch ($id) {
                case 1:
                    $crud->fields('info_satnarkoba');
                    break;
                case 2:
                    $crud->fields('info_satsabhara');
                    break;
                case 3:
                    $crud->fields('info_satintelkam');
                    break;
                case 4:
                    $crud->fields('info_satbinmas');
                    break;
                case 5:
                    $crud->fields('info_satlantas');
                    break;
                case 6:
                    $crud->fields('website_name');
                    break;
                case 7:
                    $crud->fields('logo');
                    $crud->set_field_upload('logo', 'assets/uploads/images', 'jpg|jpeg|png');
                    break;
                default:
                    redirect('configs');
                    break;
            }
        }
            
            try {
            $this->data['gc_data'] = $crud->render();
            } catch (Exception $e) {
                if ($e->getCode() == 14) { //The 14 is the code of the "You don't have permissions" error on grocery CRUD.
                    $this->load->model("config_model");
                    $rows = $this->config_model->get_all();
                    $i = 0;
                    $values = array();
                    //mencari isi tiap row
                    foreach ($rows as $row){
                        $j=1;
                        foreach ($row as $field){
                            if($field != null){
                                //jika tidak kosong, dan isinya ke 2 / bukan id, masukkan ke array
                                if($j==2){
                                    $values[$i] = $field;
                                }
                                $j++;
                            }
                        }//if value = null, return belum di isi
                        if($values[$i]==null){
                            $values[$i] = "-Belum di Isi-";
                        }
                        $i++;
                    }
//                die(var_dump($data));
                    $data['info_satnarkoba'] = $values[0];
                    $data['info_satsabhara'] = $values[1];
                    $data['info_satintelkam'] = $values[2];
                    $data['info_satbinmas'] = $values[3];
                    $data['info_satlantas'] = $values[4];
                    $data['website_name'] = $values[5];
                    $data['logo'] = $values[6];
                    $this->data["main_content"] = $this->smarty->view("configs/index.html", $data, true);
                } else {
                    show_error($e->getMessage());
                }
            }
            
            $this->smarty->display($this->getLayout(), $this->data);
	}
}