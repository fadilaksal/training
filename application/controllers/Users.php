<?php

date_default_timezone_set("Asia/Jakarta");

class Users extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->auth->logged_in()) {
            redirect('auth');
        }
        $this->data['users_menu'] = true;
        $this->data['page'] = "Users";
    }

    function index() {
        $this->load->model('member_model');
        $this->member_model->update_by(array('viewed'=>'false'),array('viewed'=>'true'));
        
        $crud = new grocery_CRUD();
        $crud->set_table('members');
        $crud->unset_jquery();
        $crud->columns('name', 'email', 'telp', 'alamat', 'created_at', 'status');
        $crud->display_as('name', "Nama")->display_as('telp', 'No. Telpon')->display_as('updated_at', 'Diperbaharui pada')
                ->display_as('created_at', 'Dibuat pada');
        
         $crud->set_field_upload('image','assets/uploads/images', 'jpg|jpeg|png');
        
        $crud->add_fields('name', 'email', 'password', 'verify_password', 'image','telp', 'imei', 'session', 'activation_code', 'alamat', 'status', 'tipe');

        $crud->unset_edit_fields('password', 'verify_password');

        $crud->field_type('password', 'password')->field_type("created_at", "invisible")
                ->field_type("updated_at", "invisible")->field_type('verify_password', 'password')
                ->field_type("session", "invisible")
                ->field_type("activation_code", "invisible");

        if ($crud->getState() == "read") {
            //hide password
            $crud->unset_read_fields('password', 'verify_password');
        } else if ($crud->getState() === "insert_validation") {
            $crud->set_rules('password', 'Password', 'trim|required|matches[verify_password]');
            $crud->set_rules('verify_password', 'Verify Password', 'required');
        }

        $crud->unset_texteditor('alamat');
        $crud->callback_column("status", array($this, "status_action"));

        $crud->required_fields('name', 'email', 'telp', 'imei', 'status');
        $crud->set_rules('email', 'Email', 'valid_email|required')
                ->set_rules('name', 'Nama', 'trim|required')
                ->set_rules('imei', 'Imei', 'trim|required')
                ->set_rules("telp", "No. Telpon", "numeric|required");

        $crud->add_action('Change Password', '', 'users/change_password', 'ui-icon-image');

        $crud->callback_before_update(array($this, "formating_date"));
        $crud->callback_before_insert(array($this, "formating_pass"));
        $crud->callback_after_upload(array($this, 'after_upload'));

        $this->data["gc_data"] = $crud->render();
        $this->smarty->display($this->getLayout(), $this->data);
    }

    //fomat date for update_at when data edited
    function formating_date($post_array) {
        $post_array = $this->formating_pass($post_array);
        $post_array['updated_at'] = date('Y-m-d H:i:s');
        return $post_array;
    }

    function formating_pass($post_array) {
        unset($post_array['verify_password']);
        if (isset($post_array['password'])) {
            $post_array['password'] = md5($post_array['password']);
        }

        $post_array['activation_code'] = $this->generateRandomString(25);
        $post_array['session'] = $this->generateRandomString(32);
        
        $post_array['viewed'] = 'true';
        return $post_array;
    }

    function status_action($primary_key, $row) {
        $id = $row->id;
        $this->load->model("member_model");
        $users = $this->member_model->get($id);

        if ($users->status == "Y") {
            return '<a href="users/banned/' . $row->id . '">Activated</a>';
        } else {
            return '<a href="users/activated/' . $row->id . '">Banned</a>';
        }
    }

    function banned() {
        echo "<script>"
        . "var check = confirm('Apakah anda ingin menonaktifkan user ini');"
        . "if(check === true){";
        $this->load->model("member_model");
        $id = (int) $this->uri->segment(3);
        if ($id > 0) {
            $this->member_model->update($id, array('status' => 'N'));
        }
        echo "window.location.href='" . base_url() . "users';"
        . "}else{";
        echo "window.location.href='" . base_url() . "users';"
        . "}"
        . "</script>";
    }

    function activated() {
        echo "<script>"
        . "var check = confirm('Apakah anda ingin mengaktifkan user ini');"
        . "if(check === true){";
        $this->load->model("member_model");
        $id = (int) $this->uri->segment(3);
        if ($id > 0) {
            $this->member_model->update($id, array('status' => 'Y'));
        }
        echo "window.location.href='" . base_url() . "users';"
        . "} else{";
        echo "window.location.href='" . base_url() . "users';"
        . "}"
        . "</script>";
    }

    function change_password() {
        $crud = new Grocery_CRUD();
        $crud->set_table('members');
        $crud->unset_list();
        $crud->unset_add();
        $crud->unset_back_to_list();

        $crud->fields('password', 'verify_password', 'updated_at');
        $crud->field_type('password', 'password')
                ->field_type("updated_at", "invisible")->field_type('verify_password', 'password');

        $crud->required_fields('password', 'verify_password');
        $crud->set_rules('password', 'Password', 'trim|required|matches[verify_password]');

        $crud->callback_before_update(array($this, 'formating_pass'));

        $id = (int) $this->uri->segment(3);
        try {
            $this->data["gc_data"] = $crud->render();
        } catch (Exception $ex) {
            if ($ex->getCode() == 14) {
                redirect('users/change_password/edit/' . $id);
            }
        }

        $this->smarty->display($this->getLayout(), $this->data);
    }

    function after_upload($uploader_response,$field_info, $files_to_upload){
        $this->load->library('image_moo');
        $x = 300; $y = 300;
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
 
        $this->image_moo->load($file_uploaded)->resize($x,$y)->save($file_uploaded,true);
        if($this->image_moo->errors) print $this->image_moo->display_errors();
    }
}
