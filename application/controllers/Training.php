<?php

class Training extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->auth->logged_in()) {
            redirect('auth', 'refresh');
        }
        $this->data["training_menu"] = "active";
        $this->data['page'] = "Training";
    }

    public function index() 
    {
        $this->data['module']       = "list";
        $this->data['category']     = "all";
        $this->data['dataTraining'] = $this->training_m->_select("id, nama, DATE_FORMAT(tanggal,'%d-%m-%Y') as tanggal, peserta, jenis")->_order_by('tanggal', 'ASC')->get_all();
        $this->data["main_content"] = $this->smarty->view("training/training.html", $this->data, true);

        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function jenis($param1 = ''){
        $this->data['module']       = "list";
        $this->data['category']     = $param1;
        $this->data['dataTraining'] = $this->training_m->_select("id, nama, DATE_FORMAT(tanggal,'%d-%m-%Y') as tanggal, peserta, jenis")->_order_by('tanggal', 'ASC')->get_many_by(array('jenis'=>$param1));
        $this->data["main_content"] = $this->smarty->view("training/training.html", $this->data, true);

        $this->smarty->display($this->getLayout(), $this->data);   
    }

    /* param2 = berisi 'departemen' */
    public function id($trainingId = 0, $param2 = null, $namaDepartemen = '')
    {
        if((int)$trainingId <= 0){
            redirect(base_url() . 'dashboard');
        }
        
        $dataTraining = $this->training_m->get($trainingId);
        
        if($dataTraining == null){
            redirect(base_url() . 'dashboard');
        }

        if($param2 == null){
            if($dataTraining->jenis_trainer == 'internal'){
                $trainerData = $this->trainer_m->getTrainerByIdTraining($trainingId)->result_array();
                if($trainerData == null){
                    $dataTraining->trainer = 'Belum Dimasukkan';
                }else{
                    $j = 0;
                    foreach ($trainerData as $field => $isi) {
                        $trainer[$j] = $isi['employee_name'];
                        $j++;
                    }
                    $dataTraining->trainer = $trainer;
                }
            }else{
                $trainer                = json_decode($dataTraining->trainer);
                $trainerArray           = explode('//', $trainer);
                $dataTraining->trainer  = $trainerArray;
            }

            $dataDepartemen = $this->training_departemen_m->_joinDepartemenWithCount()->get_many_by(array('training_id'=>$trainingId));
            $dataKaryawan   = $this->training_karyawan_m->_getJoinTrainingAndKaryawan($trainingId)->get_all();
            // print_r($dataDepartemen)
            foreach ($dataDepartemen as $key => $value) {
                $dataDepartemen[$key]->linkDepartemen = str_replace("/", "_", $value->namaDepartemen);
            }

            $dataTraining->tanggal = date('d-m-Y', strtotime($dataTraining->tanggal));
            $dataTraining->tanggal_akhir = date('d-m-Y', strtotime($dataTraining->tanggal_akhir));

            $this->data['dataTraining']     = $dataTraining;
            $this->data['dataDepartemen']   = $dataDepartemen;
            $this->data['dataKaryawan']     = $dataKaryawan;
            $this->data['page']             = "Training";
            $this->data['dataToJS']         = json_encode($this->data['dataDepartemen']);
            $this->data["main_content"]     = $this->smarty->view("training/training-id.html", $this->data, true);

            $this->smarty->display($this->getLayout(), $this->data);
        
        }else if($param2 == 'departemen'){
            if($namaDepartemen == null){
                redirect(base_url() . 'dashboard');
            }
            $namaDepartemen = str_replace("_", "/", urldecode($namaDepartemen));
            $dataTraining->departemen   = $namaDepartemen;
            $namaDepartemen             = $namaDepartemen;
            $dataKaryawan               = $this->training_karyawan_m->_getByDepartemenAllField($trainingId, $namaDepartemen)->get_all();
            $statistikHadir             = new stdClass();
            $statistikKelulusan         = new stdClass();
            $statistikHadir->hadir      = 0;
            $statistikHadir->tidakHadir = 0;
            $statistikHadir->jumlah     = count($dataKaryawan);
            $statistikKelulusan->lulus  = 0;
            $statistikKelulusan->tidak  = 0;
            $statistikKelulusan->jumlah = count($dataKaryawan);
            $preTestCount = 0;
            $postTestCount = 0;
            foreach ($dataKaryawan as $key => $value) {
                $value->employee_name = str_replace(array("'", "\"", "&quot;"), "&quot;", htmlspecialchars($value->employee_name) );
                if($value->kehadiran == 'tidak'){
                    $statistikHadir->tidakHadir++;
                }else{
                    $statistikHadir->hadir++;
                }
                if($value->status == 'lulus'){
                    $statistikKelulusan->lulus++;
                }else{
                    $statistikKelulusan->tidak++;
                }
            }

            $dataToJS                       = new stdClass();
            $dataToJS->dataTraining         = $dataTraining;
            $dataToJS->statistikHadir       = $statistikHadir;
            $dataToJS->statistikKelulusan   = $statistikKelulusan;
            $dataToJS->dataKaryawan         = $dataKaryawan;
            $jumlahLulusAllTraining = $this->training_karyawan_m->presentaseKelulusan($trainingId);
            $jumlahLulusAllTraining = $jumlahLulusAllTraining[0]->jumlah_lulus;
            $jumlahPeserta          = count($this->training_karyawan_m->get_many_by(array('training_id'=>$trainingId)));
            // echo $jumlahPeserta;
             // die();
            if($jumlahPeserta == 0){
                $presentaseLulus = 0;
            }else{
                $presentaseLulus = round(($jumlahLulusAllTraining / $jumlahPeserta)* 100);
            }

            $this->data['lulus']        = $presentaseLulus;
            $this->data['dataToJS']     = json_encode($dataToJS);
            $this->data['dataTraining'] = $dataTraining;
            $this->data['dataKaryawan'] = $dataKaryawan;
            $this->data['page']         = "Training Karyawan";
            $this->data["main_content"] = $this->smarty->view("training/training-departemen.html", $this->data, true);

            $this->smarty->display($this->getLayout(), $this->data);
        }else{
            redirect(base_url() . 'dashboard');
        }
    }

    public function add()
    {
        $this->data['module']           = 'add';
        $this->data['category']         = 'new';
        $this->data['dataDepartemen']   = $this->departemen_m->joinKaryawan()->get_all();
        $this->data["main_content"]     = $this->smarty->view("training/training-add.html", $this->data, true);

        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_add()
    {
        $data   = $this->input->post();

        // $dataDepartemen = $data['departemen'];
        $nama           = $data['nama'];
        $jenis          = $data['jenis'];
        $tanggal        = date("Y-m-d", strtotime($data['tanggal'])); 
        $tanggal_akhir  = date("Y-m-d", strtotime($data['tanggal_akhir'])); 
        $jam_mulai      = $data['jam_awal']; 
        $jam_selesai    = $data['jam_akhir']; 
        $durasi         = $data['durasi'];
        $lokasi         = $data['lokasi'];
        $jenisTrainer   = $data['jenisTrainer'];
        $trainer        = $data['trainer'];
        $kategori       = $data['kategori'];
        $nilai          = $data['nilai'];
        $trainer2       = '';
        $dataTrainer = array();
        foreach ($trainer as $key) {
            if($key != '')
            $dataTrainer[] = $key;
        }

        $trainer = $dataTrainer;
        if($jenisTrainer != 'internal'){
            $trainer2 = json_encode(implode("//", $trainer));
        }
        $peserta    = $data['peserta'];
        $biayaRiil  = $data['biayaRiil'];
        $biayaPlan  = $data['biayaPlan'];

        $insertData = array(
            'nama'              => $nama,
            'tanggal'           => $tanggal,
            'tanggal_akhir'     => $tanggal_akhir,
            'jam_mulai'         => $jam_mulai,
            'jam_selesai'       => $jam_selesai,
            'jenis'             => $jenis,
            'durasi'            => $durasi,
            'lokasi'            => $lokasi,
            'jenis_trainer'     => $jenisTrainer,
            'trainer'           => $trainer2,
            'peserta'           => $peserta,
            'biaya_riil'        => $biayaRiil,
            'biaya_plan'        => $biayaPlan,
            'kategori'          => $kategori,
            'nilai_kelulusan'   => $nilai
            );
        
        $insertIdTraining = $this->training_m->insert($insertData);

        if($insertIdTraining > 0){
            if($jenisTrainer == 'internal'){
                $trainerName = array();
                foreach ($trainer as $key) {
                    $trainerName[] = array(
                        'training_id' => $insertIdTraining,
                        'employee_id' => $key
                        );
                }
                $trainerId = $this->trainer_m->insert_many($trainerName);
            }

            $this->load->model('training_custom_model');
            $trainingCustom = $this->training_custom_model->get_by(array('nama'=>$nama));
            // print_r($trainingCustom);
            if($trainingCustom == null){
                $this->training_custom_model->insert(array(
                    'nama'              => $nama,
                    'jenis'             => $jenis,
                    'kategori'          => $kategori,
                ));
            }

            // $insertData = array();
            
            // foreach ($dataDepartemen as $key) {
            //     $insertData[] = array('training_id'=>$insertIdTraining, 'departemen_id' => (int)$key);
            // }
            
            // $insertIdDepartemen = $this->training_departemen_m->insert_many($insertData);
            echo 'true';
            // if($insertIdDepartemen > 0){
            //     echo 'true';
            // }else{
            //     $deleteId   = $this->trainer_m->delete_by(array('training_id'=>$insertIdTraining));
            //     $deleteId2  = $this->training_m->delete($insertIdTraining);
            //     echo 'false';
            // }
        }else{
            echo 'false';
        }
    }

    // public function add_departemen($param1 = null){
    //     $this->data['page'] = 'Tambah Training';

    //     if($param1 != null){ // jika tidak id nya tidak kosong, maka id diisi dan tidak perlu mencari training
    //         $id = (int)$param1;
    //         $dataTraining = $this->training_m->get($id);
    //         if($id<0 || $dataTraining == null){
    //             redirect(base_url() . 'dashboard');
    //         }
    //         $this->data['namaTraining'] = $dataTraining->nama;
    //         $this->data['id'] = $id;
    //     }else{ //jika tidak maka perlu mengimport ajax untuk mencari training
    //         $this->data['js_include'] = 'ajax_training_addDepartemen.js'; //script ajax ketika memencet tombol cari
    //     }
    //     $this->data['dataDepartemen'] = $this->departemen_m->joinKaryawan()->get_all();
    //     $this->data['js_script'] = 'training-addDepartemen.js'; //script untuk checkAll checkbox
    //     $this->data["main_content"] = $this->smarty->view("training/training-add-departemen.html", $this->data, true);
    //     $this->smarty->display($this->getLayout(), $this->data);
    // }

    // public function proses_add_departemen(){
    //     $idTraining = (int)$this->input->post('id');
    //     if($idTraining <= 0){
    //         die('false');
    //     }
    //     $departemen = $this->input->post('departemen');
    //     $insertData = array();
    //     foreach ($departemen as $key) {
    //          $insertData[] = array('training_id'=>$idTraining, 'departemen_id' => (int)$key);
    //     }
    //     $insertId = $this->training_departemen_m->insert_many($insertData);
    //     if($insertId > 0){
    //         echo 'true';
    //     }else{
    //         echo 'false';
    //     }
    // }

    public function add_karyawan($param1 = null)
    {
        $this->data['module'] = 'add';
        $this->data['category'] = 'karyawan';
        if($param1 != null){ // jika tidak id nya tidak kosong, maka id diisi dan tidak perlu mencari training
            $id = (int)$param1;
            $dataTraining = $this->training_m->get($id);
            if($id<0 || $dataTraining == null){
                redirect(base_url() . 'dashboard');
            }
            $this->data['namaTraining'] = $dataTraining->nama;
            $this->data['id'] = $id;
        }else{ //jika tidak maka perlu mengimport ajax untuk mencari training
            $this->data['js_include'] = 'ajax_training_addKaryawan.js'; //script ajax ketika memencet tombol cari
        }
        $this->data['dataDepartemen'] = $this->departemen_m->joinKaryawan()->get_all();
        $this->data['js_script'] = 'training-addkaryawan.js'; //script untuk checkAll checkbox
        $this->data["main_content"] = $this->smarty->view("training/training-add-karyawan.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_add_karyawan()
    {
        $idTraining = (int)$this->input->post('id');
        $idDepartemen = (int)$this->input->post('departemen');
        if($idTraining <= 0){
            die('false');
        }
        $departemen = $this->input->post('departemen');
        $karyawan = $this->input->post('karyawan');
        $insertData = array();
        foreach ($karyawan as $key) {
             $insertData[] = array('training_id'=>$idTraining, 'employee_ID' => $key);
        }
        //echo print_r($insertData);
         $insertId = $this->training_karyawan_m->insert_many($insertData);
        if($insertId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    //edit training
    public function edit($param1 = null, $param2 = 0)
    {
        $this->data['js_include'] = 'ajax_training_edit.js';
        if($param1 == null){ //jika belum memilih training untuk diedit
            $this->data['module']       = 'edit';
            $this->data["main_content"] = $this->smarty->view("training/training-edit.html", $this->data, true);

            $this->smarty->display($this->getLayout(), $this->data);

        }else if($param1 == 'id'){ //jika edit sudah dipilih training nya
            if($param2 <= 0){
                redirect(base_url() . 'dashboard');
            }
            $data = $this->training_m->get($param2);
            if($data == null){
                redirect(base_url() . 'dashboard');
            }
            $data->tanggal = date("Y-m-d", strtotime($data->tanggal)); 
            $this->data['dataTraining'] = $data;
            $this->data['page'] = 'Tambah Training';
            $this->data['js_script'] = 'inputDate.js';
            $this->data["main_content"] = $this->smarty->view("training/training-edit-id.html", $this->data, true);
            $this->smarty->display($this->getLayout(), $this->data);
        }else{
            redirect(base_url() . 'dashboard');
        }
    }

    public function proses_edit()
    {
        $data = $this->input->post();

        $tanggal            = $this->input->post('tanggal');
        $tanggal            = str_replace('/', '-', $tanggal);
        $tanggal            = date("Y-m-d", strtotime($tanggal)); 
        $data['tanggal']    = $tanggal;

        $updateId = $this->training_m->update($data['id'], $data);
        if($updateId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function proses_edit_training_karyawan()
    {
        $data = $this->input->post();
        $id = (int)$data['id'];
        $idTraining = (int)$data['idTraining'];
        if($id <= 0 || $idTraining <= 0){
            echo 'false';
            return;
        }
        $trainingData = $this->training_m->get($idTraining);
        // $kehadiran =  $data['kehadiran'];
        $pretest = 0;
        $posttest = 0;
        $status = 'tidak lulus';
        if($trainingData->kategori != 'sosialisasi'){
            $posttest = $data['posttest'];
        }
        
        if($trainingData->kategori == 'training'){
            $pretest = $data['pretest'];
            if($pretest > $trainingData->nilai_kelulusan){
                $status = 'lulus';
            }
        }else if($trainingData->kategori == 'refresh'){
            if($posttest > $trainingData->nilai_kelulusan){
                $status = 'lulus';
            }
        }

        $updateId = $this->training_karyawan_m->update($id, array('kehadiran' => 'hadir', 'pre_test'=>$pretest, 'post_test' => $posttest, 'status' => $status));
        if($updateId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function editPersenKepuasan(){
        $id         = (int)$this->input->post('training_id');
        $kepuasan   = $this->input->post('persen_kepuasan');
        $updateId   =  $this->training_m->update($id, array('persen_kepuasan'=>$kepuasan), TRUE);
        
        if($updateId > 0){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function hapus($param1 = '')
    {
        $id = $this->input->post('id');
        $deleteId = $this->training_m->delete($id, TRUE);
        if($deleteId > 0){
            $deleteTrainingDepartemenId = $this->training_departemen_m->delete_by(array('training_id'=>$id));
            $deleteTrainingKaryawanId   = $this->training_karyawan_m->delete_by(array('training_id'=>$id));
            $deleteTrainerId            = $this->trainer_m->delete_by(array('training_id'=>$id));
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function absen()
    {
        $this->data["dataTraining"] = $this->training_m->get_all();
        $this->data["main_content"] = $this->smarty->view("training/training-absensi.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_absen()
    {
        $idTraining = $this->input->post('idTraining');
        $trainingData = $this->training_m->get($idTraining);
        $isSuccess  = false;
        $path       = base_url() . 'assets/upload/csv';

        $config['upload_path']      = 'assets/upload/csv';
        $config['allowed_types']    = 'csv';
        $config['max_size']         = '1000';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('dataAbsensi')){
            $this->data['error'] = $this->upload->display_errors();
        }else{
            $fileData = $this->upload->data();
            $filePath = 'assets/upload/csv/' . $fileData['file_name'];
            $status = 'tidak lulus';
            if($trainingData->nilai_kelulusan == 0){
                $status = 'lulus';
            }

            if($this->csvimport->get_array($filePath)){
                $csvArray   = $this->csvimport->get_array($filePath);
                $data       = array();
                
                $jam_mulai   = date('H:i:s', strtotime($trainingData->jam_mulai));
                $jam_selesai = date('H:i:s', strtotime($trainingData->jam_selesai));
                $i = 0;
                foreach ($csvArray as $row) {
                    $rowExport = explode(';', $row['absen']);
                    $time = date('H:i:s', strtotime($rowExport[3]));
                    if($time < $jam_mulai || $time > $jam_selesai){
                        continue;
                    }
                    if($rowExport[0] == null || $rowExport[0] == '' ){
                        continue;
                    }
                    // echo "$time <br>";
                    if($i != 0 && $data[$i-1]['employee_id'] != $rowExport[0]){ //jika data sebelumnya tidak sama dengan data setelahnya
                        $data[$i] = array(
                            'employee_id' => $rowExport[0],
                            'training_id' => $idTraining,
                            'status' => $status
                        );
                    }else{
                        $data[$i] = array(
                        'employee_id' => $rowExport[0],
                        'training_id' => $idTraining
                        );
                    }
                    $i++;
                }
                // die();

                $insertId = $this->training_karyawan_m->insert_many($data);
                $insertId =1;
                if($insertId > 0){
                    $trainingData   = $this->training_m->get($idTraining);
                    $manhour        = $i * $trainingData->durasi;
                    $trainingId     = $this->training_m->update($idTraining, array('manhour'=>$manhour));
                    $departemenData = $this->training_karyawan_m->_getDepartemenRightJoin($idTraining)->get_all();
                    
                    $departemenId = array();
                    foreach ($departemenData as $key => $value) {
                        if($value->id_departemen != null && $value->id_departemen != ''){
                            $departemenId[] = array(
                                'training_id' => $idTraining,
                                'departemen_id' => $value->id_departemen
                            );
                        }
                    }

                    $departemenInsertId = $this->training_departemen_m->insert_many($departemenId);
                    
                    $isSuccess      = true;
                }else{
                    $isSuccess = false;
                }
            }else{
                $isSuccess = false;
            }

            unlink($filePath);

            $this->data['isSuccess']    = $isSuccess;
            $this->data["dataTraining"] = $this->training_m->get_all();
            $this->data["main_content"] = $this->smarty->view("training/training-absensi.html", $this->data, true);

            $this->smarty->display($this->getLayout(), $this->data);
        }
    }

    public function export_all()
    {
        $dataField      = $this->training_m->getTraining()->list_fields();
        $dataTraining   = $this->training_m->get_all();
        $trainer        = array();

        $i = 0;
        foreach ($dataTraining as $key => $value) {
            if($value->jenis_trainer == 'internal'){
                $trainerData = $this->trainer_m->getTrainerByIdTraining($value->id)->result_array();
                if($trainerData == null){
                    $value->trainer = ' ';
                }else{
                    $j = 0;
                    foreach ($trainerData as $field => $isi) {
                        $trainer[$i][$j] = $isi['employee_name'];
                        $j++;
                    }
                    $string         = implode(', ', $trainer[$i]);
                    $value->trainer = $string;
                } $i++;
            }else{
                $string         = json_decode($value->trainer);
                $trainerArray   = explode('//', $string);
                $trainerString  = implode(', ', $trainerArray);
                $value->trainer = $trainerString;
            }
        }

        $this->load->library('excel');
        //set cell A1 content with some text
        $this->export($dataField, $dataTraining, 'List Training All');
    }

    public function export_id($idTraining)
    {
        $this->data['module']   = 'list';
        $dataField              = $this->training_departemen_m->_joinDepartemenWithCountFieldForExportId($idTraining)->list_fields();
        $dataDepartemen         = $this->training_departemen_m->__joinDepartemenWithCountForExportId()->get_many_by(array('training_id'=>$idTraining));
        $dataTraining           = $this->training_m->get($idTraining);

        $this->load->library('excel'); 

        $namaFile = "List Departemen Training $dataTraining->nama";

        $this->export($dataField, $dataDepartemen, $namaFile);
    }

    public function export_id_all($idTraining, $namaTraining = '')
    {
        $dataDepartemen = $this->training_departemen_m->_joinDepartemenWithCount()->get_many_by(array('training_id'=>$idTraining));
        $dataTraining   = array();

        foreach ($dataDepartemen as $key => $value) {
            $dataTraining["$value->namaDepartemen"] = new stdClass();
        }

        foreach ($dataTraining as $key => $value) {
            $dataTraining["$key"] = $this->training_karyawan_m->_getByDepartemenWithTraining($idTraining, $key)->get_all();
        }

        $dataDepartemenField    = $this->training_karyawan_m->_getByDepartemenField($idTraining)->list_fields();
        $dataField              = array();

        $dataField[0]   = 'No';
        $dataField[1]   = 'No Register';
        $dataField[2]   = 'Nama';
        $dataField[3]   = 'Departemen';
        $dataField[4]   = 'Tanggal';
        $dataField[5]   = 'Trainer';
        $dataField[6]   = 'Durasi';
        $dataField[7]   = 'Manhours';
        $dataField[8]   = 'Nilai Rata2 Pre Test';
        $dataField[9]   = 'Nilai Rata2 Post Test';
        $dataField[10]  = 'tidak lulus';

        $dataTrainingAll = array();

        foreach ($dataTraining as $key => $dataDepartemenNew) {
            $dataTrainingAll["$key"] = $key;
            $dataTrainingAll["$key"] = array();

            $i = 0;
            foreach ($dataDepartemenNew as $field => $value) {
                $dataTrainingAll["$key"][$i][0] = $value->employee_ID;
                $dataTrainingAll["$key"][$i][1] = $value->employee_name;
                $dataTrainingAll["$key"][$i][2] = $value->department;
                $dataTrainingAll["$key"][$i][3] = $value->tanggal;

                $this->changeJenis($value, $value->training_id);

                $dataTrainingAll["$key"][$i][4] = $value->trainer;
                $dataTrainingAll["$key"][$i][5] = $value->durasi;
                $dataTrainingAll["$key"][$i][6] = $value->durasi * count($dataDepartemenNew);
                $dataTrainingAll["$key"][$i][7] = $value->pre_test;
                $dataTrainingAll["$key"][$i][8] = $value->post_test;
                $dataTrainingAll["$key"][$i][9] = $value->status;
                $i++;
            }
        }

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 15,
                'name'  => 'Verdana'
        ));

        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleHeadArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '92D050'
                ),
                'endcolor' => array(
                    'rgb' => '92D050'
                ),
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '000000')
                )
            )
        );

         $sheet = 0;
         foreach ($dataTrainingAll as $departemen => $dataDepartemen) {
            $objPHPExcel->createSheet($sheet);
            $objPHPExcel->setActiveSheetIndex($sheet);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->getStyle('A3:K3')->applyFromArray($styleHeadArray);
            $objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B1:E1');
            
            $departemen = urldecode($departemen);
            
            $departemen = substr($departemen, 0, 30);
            $objPHPExcel->getActiveSheet()->setTitle($departemen);
            
            $col = 0;

            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "JUDUL : ");
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, urldecode($namaTraining));
            foreach ($dataField as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 3, $value);
                $col++;
            }

            $row    = 4;
            $no     = 1;
            foreach ($dataDepartemen as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $no);

                $col = 1;
                foreach ($field as $value) {
                    if($col == 5){
                        $value = implode(',', $value);
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $row++;
                $no++;
            }
            $sheet++;
         }
        
        $filename= "List $namaTraining.xlsx"; 
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
        $objWriter->save('php://output');
        exit();
        ob_end_clean();
    }

    private function export($fieldName, $data, $namaFile)
    {
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('List Training Garuda Food');

        foreach(range('A','G') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $col = 0;
        foreach ($fieldName as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $value);
            $col++;
        }

        $row = 2;
        foreach ($data as $key) {
            $col = 0;
            foreach ($key as $value) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                $col++;
            }
            $row++;
        }

        $filename= $namaFile . '.xlsx'; 
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
        $objWriter->save('php://output');
        exit();
        ob_end_clean();
    }

    //untuk jenis trainer
    private function changeJenis($object, $param1)
    {
        if($object->jenis_trainer == 'internal'){
            $trainer = array();
            $trainerData = $this->trainer_m->getTrainerByIdTraining($param1)->result_array();
            if($trainerData == null){
                $object->trainer = '-';
            }else{
                $j = 0;
                foreach ($trainerData as $field => $isi) {
                    $trainer[$j] = $isi['employee_name'];
                    $j++;
                }
                $object->trainer = $trainer;
            }
        }else{
            //$trainerArray = array();
            $trainer = json_decode($object->trainer);
            $trainerArray = explode('//', $trainer);
            $object->trainer = $trainerArray;
            //die(print_r($data->trainer));
        }
    }

    public function exportTraining($param1 = 0, $param2 = '')
    {
        if($param1 == 0){
            redirect(base_url() . 'training');
        }
        $dataTraining = $this->training_m->get($param1);
        $dataDepartemen = $this->training_departemen_m->_joinDepartemenWithCount()->get_many_by(array('training_id'=>$param1));
        $param3 = urldecode($param2);
        $dataKaryawan = $this->training_karyawan_m->_getByDepartemenAllField($param1, $param3)->get_all();
        foreach ($dataKaryawan as $key => $value) {
             $value->employee_name = str_replace(array("'", "\"", "&quot;"), "&quot;", htmlspecialchars($value->employee_name) );
            $dataTraining->departemen = $param3;
            $dataKaryawan = $this->training_karyawan_m->_getByDepartemenAllField2($param1, $param3)->get_all();
            $dataField = $this->training_karyawan_m->_getByDepartemenAllJustField($param1, $param3)->list_fields();

            $this->load->library('excel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('List Training Garuda Food');
            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $col = 0;
            foreach ($dataField as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $value);
                $col++;
            }

            $row = 2;
            foreach ($dataKaryawan as $key) {
                $col = 0;
                foreach ($key as $value) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $row++;
            }
            if($param3 != ''){
                $filename= $param3; 
            }else{
                $filename = $dataTraining->nama;
            }
            ob_end_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
            header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"'); 
            header('Cache-Control: max-age=0'); 
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
            
            $objWriter->save('php://output');
            exit();
            ob_end_clean();
        }
    }

     public function upload_nilai()
     {
        $training_id = $this->input->post('training_id');

        $path = base_url() . 'assets/upload/csv';

        $config['upload_path']      = 'assets/upload/csv';
        $config['allowed_types']    = 'csv';
        $config['max_size']         = '1000';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('dataNilai')){
            $this->data['error'] = $this->upload->display_errors();
            die($this->data['error']);
        }else{
            $fileData   = $this->upload->data();
            $filePath   = 'assets/upload/csv/' . $fileData['file_name'];
            $isSuccess  = false;
            
            $training_data = $this->training_m->get($training_id);

            if($this->csvimport->get_array($filePath)){
                $csvArray       = $this->csvimport->get_array($filePath);
                $nilai          = array();
                $employee_ID    = array();
                foreach ($csvArray as $row) {
                    $status = 'tidak lulus';
                    if($row['employee_ID'] == '' || $row['employee_ID'] == null){
                        continue;
                    }
                    if($training_data->nilai_kelulusan != 0){
                        if($training_data->kategori == 'training'){
                            if($row['pre_test'] >= $training_data->nilai_kelulusan && $row['post_test'] == 0){
                                $status = 'lulus';
                            }else if($row['post_test'] >= $training_data->nilai_kelulusan){
                                $status = 'lulus';
                            }
                        }else if($training_data->kategori == 'refresh'){
                            if($row['post_test'] >= $training_data->nilai_kelulusan){
                                $status = 'lulus';
                            }
                        }
                    }else{
                        $status = 'lulus';
                    }
                    $nilai[] = array(
                        'training_id'   => $training_id,
                        'employee_ID'   =>  $row['employee_ID'],
                        'pre_test'      => $row['pre_test'],
                        'post_test'     => $row['post_test'],
                        'status'        => $status
                        );
                }

                $updateId = $this->training_karyawan_m->update_multiple($nilai);
                if($updateId > 0){
                    $isSuccess = true;
                }else{
                    $isSuccess = false;
                }
            }else{
                $isSuccess = false;
            }

            unlink($filePath);

            header("location:" . base_url() . "training/id/$training_id");
        }
    }

    //mengambil data training(Ajax)
    public function getTrainingForEdit()
    {
        $nama   = $this->input->post('nama');
        $data   = $this->training_m->_like('nama', $nama)->get_all();
        $count  = $this->training_m->_like('nama', $nama)->count_all();

        $this->data['dataTraining'] = $data;
        $this->data['jumlahData']   = $count;
        $this->data['btnLink']      = base_url() . 'training/edit/id/';
        $this->data['btnCaption']   = 'Edit';
        $this->data['btnClass']     = 'fa fa-pencil';

        $this->smarty->display("training/training-data.html", $this->data);
    }

    //mengambil data training untuk add departemen
    public function getTrainingForAdd()
    {
        $nama   = $this->input->post('nama');
        $data   = $this->training_m->_like('nama', $nama)->get_all();
        $count  = $this->training_m->_like('nama', $nama)->count_all();
        $this->data['dataTraining'] = $data;
        $this->data['jumlahData']   = $count;
        $this->data['btnLink']      = '';
        $this->data['btnCaption']   = 'Pilih';
        $this->data['btnClass']     = 'fa fa-eye';

        $this->smarty->display("training/training-data.html", $this->data);
    }

    public function getDepartemenForTraining()
    {
        $id = $this->input->post('id');
        $trainingData = $this->training_departemen_m->_joinDepartemen()->get_many_by(array('training_id'=>$id));
        
        echo "<option value='0'>Pilih Departemen</option>";
        foreach ($trainingData as $key => $value) {
            echo "<option value='$value->nama'>$value->nama</option>";
        }
    }

    public function getKaryawanForTraining()
    {
        $departemen     = $this->input->post('departemen');
        $karyawanData   = $this->karyawan_m->get_many_by(array('department' => $departemen));

        $this->data['dataKaryawan'] = $karyawanData;

        $this->smarty->display("karyawan/karyawan-data-check.html", $this->data);
    }

    public function getCustomTraining(){
        $nama = $this->input->post('nama');
        $this->load->model('training_custom_model');
        $trainingData = $this->training_custom_model->get_by(array('nama'=>$nama));
        $return = json_encode($trainingData);
        echo $return;
    }
}