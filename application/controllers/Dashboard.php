<?php

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        if (!$this->auth->logged_in()) {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }
        $this->data["dashboard_menu"] = "true";
    }

    public function index() {
        $this->data['page'] = "Dashboard";
        $this->data['js_script'] = 'dashboard.js';
        $this->data['countTraining'] = $this->training_m->count_all();
        $this->data['countDepartemen'] = $this->departemen_m->count_all();
        $this->data['countKaryawan'] = $this->karyawan_m->count_all();
        $totalBiaya = $this->training_m->selectBiayaPerTahun(2017)->result();
        if($totalBiaya != null){
        $this->data['totalBiaya'] = $totalBiaya[0]->total;
        }else{
            $this->data['totalBiaya'] = 0;
        }
        //echo print_r($totalBiaya);
        //die();
        $this->data['lastFiveTraining'] = $this->training_m->_limit(5, 0)->_order_by('tanggal', 'DESC')->get_all();
        $this->data['lastYearTraining'] = $this->training_m->_select("DATE_FORMAT(tanggal, '%m') as bulanAngka, DATE_FORMAT(tanggal,'%M') as bulan, count(tanggal) as 'jumlah'")->group_by("DATE_FORMAT(tanggal,'%m')")->order_by('bulanAngka', 'ASC')->get_all();
        $this->data['bigFiveDepartment'] = $this->departemen_m->selectWithTrainingCount()->get_all();
        $dataToJS = new stdClass();
        //$dataToJS->lastFiveTraining = $this->data['lastFiveTraining'];
        $dataToJS->lastYearTraining = $this->data['lastYearTraining'];
        $dataToJS->bigFiveDepartment = $this->data['bigFiveDepartment'];
        $this->data['dataToJS'] = json_encode($dataToJS);
        $this->data["main_content"] = $this->smarty->view("dashboard.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function export_training(){
    	$dataField = $this->training_m->getTraining()->list_fields();
        $dataTraining = $this->training_m->get_all();
        $trainer = array();
        $i = 0;
        foreach ($dataTraining as $key => $value) {
        	if($value->jenis_trainer == 'internal'){
        		$trainerData = $this->trainer_m->getTrainerByIdTraining($value->id)->result_array();
        		if($trainerData == null){
        			$value->trainer = ' ';
        		}else{
        			$j = 0;
        			foreach ($trainerData as $field => $isi) {
        				$trainer[$i][$j] = $isi['employee_name'];
        				$j++;
        			}
        			$string = implode(', ', $trainer[$i]);
        			$value->trainer = $string;
        		} $i++;
        	}else{
        		$string = json_decode($value->trainer);
        		$trainerArray = explode('//', $string);
        		$trainerString = implode(', ', $trainerArray);
        		$value->trainer = $trainerString;
        	}
        }

        $this->load->library('excel');
        //set cell A1 content with some text
        $this->export($dataField, $dataTraining, 'List Training All');
    }

    private function export($fieldName, $data, $namaFile){
        $objPHPExcel = new PHPExcel();
        //activate worksheet number 1
        $objPHPExcel->setActiveSheetIndex(0);
        //name the worksheet
        $objPHPExcel->getActiveSheet()->setTitle('List Training Garuda Food');
        foreach(range('A','G') as $columnID) {
	        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }
        $col = 0;
        foreach ($fieldName as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $value);
            $col++;
        }

        $row = 2;
        foreach ($data as $key) {
            $col = 0;
            foreach ($key as $value) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, 'Total Biaya');
        $total = $this->training_m->selectBiayaPerTahun(2017)->result();
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row+1, $total[0]->total);
        $filename= $namaFile . '.xls'; //save our workbook as this file name
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                    
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        ob_end_clean();
    }
}
