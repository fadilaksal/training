<?php

class Karyawan extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->auth->logged_in()) {
            // redirect them to the login page
            redirect('auth', 'refresh');
        }
        $this->data["karyawan_menu"] = "active";
    }

    public function index() {
        $this->data['page'] = "Karyawan";
        $dataKaryawan = $this->karyawan_m->get_all();
        $this->data['dataKaryawan'] = $dataKaryawan;
        $this->data["main_content"] = $this->smarty->view("karyawan/karyawan.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function id($param1 = 0){
        $id = (int)$param1;
        if($id == 0){
            redirect(base_url() . 'dashboard');
        }
        $dataTraining = $this->training_karyawan_m->_joinTraining()->get_many_by(array('employee_id'=> $id));
        $dataTrainer = $this->trainer_m->getTrainingByIdKaryawan($id)->result();
        $jumlahDurasi = $this->trainer_m->getTotalDurasi($id)->result()[0]->total;
        //echo $this->db->last_query() . '<br>';
        //die(var_dump($dataTraining2));
        //die($jumlahDurasi);
        $this->data['dataTrainer'] = $dataTrainer;
        $this->data['dataTraining'] = $dataTraining;
        $this->data['dataKaryawan'] = $this->karyawan_m->get($id);
        $this->data["main_content"] = $this->smarty->view("karyawan/karyawan-id.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function departement($param1 = null){
        if($param1 == null){
            redirect(base_url() . 'dashboard');
        }else{
            $this->data['page'] = "Karyawan";
            $dataKaryawan = $this->karyawan_m->get_many_by(array('department' => $param1));
            $this->data['dataKaryawan'] = $dataKaryawan;
            $this->data["main_content"] = $this->smarty->view("karyawan/karyawan.html", $this->data, true);
            $this->smarty->display($this->getLayout(), $this->data);
        }
    }

    public function upload(){
        $this->data['page'] = "Karyawan";
        $this->data['js_include'] = 'ajax_upload_karyawan.js';
        $this->data["main_content"] = $this->smarty->view("karyawan/karyawan-upload.html", $this->data, true);
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_upload(){
        $path = base_url() . 'assets/upload/csv';
        $config['upload_path'] = 'assets/upload/csv';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('dataKaryawan')){
            $this->data['error'] = $this->upload->display_errors();
            //echo 'false';
             die($this->data['error']);
            // $this->data["main_content"] = $this->smarty->view("karyawan/karyawan-upload.html", $this->data, true);
            // $this->smarty->display($this->getLayout(), $this->data);
        }else{
            $fileData = $this->upload->data();
            $filePath = 'assets/upload/csv/' . $fileData['file_name'];

            $this->karyawan_m->_truncate();

            $isSuccess = false;

            if($this->csvimport->get_array($filePath)){
                $csvArray = $this->csvimport->get_array($filePath);
                $data = array();
                foreach ($csvArray as $row) {
                    $data[] = array(
                        'employee_ID' => $row['Employee ID'],
                        'employee_Name' => $row['Employee Name'],
                        'department' => $row['Department'],
                        'position_id' => $row['Position ID'],
                        'position_name' => $row['Position Name'],
                        'line' => $row['Organization']
                        );
                }

                $insertId = $this->karyawan_m->insert_many($data);
                if($insertId > 0){
                    $isSuccess = true;
                }else{
                    $isSuccess = false;
                }
            }else{
                $isSuccess = false;
            }
            unlink($filePath);
            $this->data['isSuccess'] = $isSuccess;
            $this->data['js_include'] = 'ajax_upload_karyawan.js';
            $this->data["main_content"] = $this->smarty->view("karyawan/karyawan-upload.html", $this->data, true);
            $this->smarty->display($this->getLayout(), $this->data);
        }
    }



    public function getKaryawanById($id = 0){
        $karyawanData = $this->karyawan_m->get($id);
        if($karyawanData != null){
            echo $karyawanData->employee_name;
        }else{
            echo '-';
        }
    }

}
