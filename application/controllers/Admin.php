<?php

class Admin extends MY_Controller {

    function __construct() 
    {
        parent::__construct();
        if (!$this->auth->logged_in()) {
            redirect('auth');
        }
        $this->data['page'] = "Admin";
        $this->load->model('ion_auth_model');
    }

    public function index() 
    {
        $this->data['adminData'] = $this->ion_auth_model->users()->select('username', 'email', 'first_name', 'last_name', 'created_on')->result();
        foreach ($this->data['adminData'] as $key => $value) {
            $date = $value->created_on;
            $value->created_on = date('d/m/Y', $date);
        }
        $this->data['main_content'] = $this->smarty->view('configs/admin.html', $this->data, true);
        
        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function edit($id)
    {
        $this->data['js_include'] = 'ajax_account_edit.js';
        if((int)$id < 1){
            header('location: ' . base_url());
        }
        $this->data['adminData']    = $this->ion_auth_model->user($id)->select('username', 'email', 'first_name', 'last_name', 'created_on')->result()[0];
        $this->data['main_content'] = $this->smarty->view('configs/edit.html', $this->data, true);

        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_edit($id = 0)
    {
        $id = $this->input->post('id');
        if($id < 0){
            redirect(base_url() . 'account');
        }
        $username   = $this->input->post('username');
        $firstname  = $this->input->post('firstname');
        $lastname   = $this->input->post('lastname');

        $updateData = array(
            'username'      => $username,
            'first_name'    => $firstname,
            'last_name'     => $lastname
            );

        if($this->ion_auth_model->update($id, $updateData)){
            echo 'true';
        }else{
            echo 'false';
        }
    }

    public function add()
    {
        $this->data['js_include']   = 'ajax_account_add.js';
        $this->data['main_content'] = $this->smarty->view('configs/add.html', $this->data, true);

        $this->smarty->display($this->getLayout(), $this->data);
    }

    public function proses_add()
    {
        $email              = strtolower($this->input->post('email'));
        $username           = $this->input->post('username');
        $firstname          = $this->input->post('firstname');
        $lastname           = $this->input->post('lastname');
        $password           = $this->input->post('password');
        $confirmation       = $this->input->post('confirmation');
        $identity_column    = $this->config->item('identity','ion_auth');

        if($password != $confirmation){
            echo '<script>alert("Password dan Password Konfirmasi tidak sama");location.href="'.base_url().'admin"</script>';
        }

        $identity = ($identity_column==='email') ? $email : $username;

        $additional_data = array(
            'username' => $username,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'created_on' => date('Y-m-d H:i:s')
        );
            
        if($this->auth->register($identity, $password, $email, $additional_data)){
            echo 'true';
        }else{
            echo $this->auth->errors();
        }
    }

    public function delete(){
        $id = (int)$this->input->post('id');
        $deleteId = $this->ion_auth_model->delete_user($id);
        if($deleteId){
            echo 'true';
        }else{
            echo 'false';
        }
    }
    
    function change_password() 
    {
        $data = array('password' => $values['password']);
        if($this->auth->update($primary_key, $data)){
            return true;
        }else{
            return false;
        }
    }

    function edit_password(){
        // print_r($this->input->post());
        $email      = trim($this->input->post('email'));
        $old        = trim($this->input->post('old_pass'));
        $new        = trim($this->input->post('new_pass'));

        $editId = $this->ion_auth_model->change_password($email, $old, $new);
        // echo $editId;
        if($editId){
            echo 'true';
        }else{
            echo 'false';
        }
    }
}
