<?php
class MY_Controller extends CI_Controller {

    var $data;
    var $user;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->data['departemenList'] = $this->departemen_m->get_all();
        // die(print_r($this->data['departemenList']));
    }

    public function getLayout() {
        return "layout.html";
    }

    public function getUser() {
        return $this->auth->user()->row();
    }
    
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function translateBulan($bulan = ''){
        if($bulan == 'january'){
            return $bulan = 'januari';
        }else if($bulan == 'february'){
            return $bulan = 'februari';
        }else if($bulan == 'march'){
            return $bulan = 'maret';
        }else if($bulan == 'april'){
            return $bulan = 'april';
        }else if($bulan == 'mei'){
            return $bulan = 'mei';
        }else if($bulan == 'june'){
            return $bulan = 'juni';
        }else if($bulan == 'july'){
            return $bulan = 'juli';
        }else if($bulan == 'august'){
            return $bulan = 'agustus';
        }else if($bulan == 'september'){
            return $bulan = 'september';
        }else if($bulan == 'october'){
            return $bulan = 'oktober';
        }else if($bulan == 'november'){
            return $bulan = 'november';
        }else if($bulan == 'december'){
            return $bulan = 'desember';
        }
    }
}
